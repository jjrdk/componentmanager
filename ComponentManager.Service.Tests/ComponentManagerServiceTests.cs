﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentManagerServiceTests.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ComponentManagerServiceTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Service.Tests
{
	using System;
	using System.Diagnostics.CodeAnalysis;
	using System.IO;
	using System.Net;
	using System.ServiceModel;
	using System.Threading.Tasks;

	using ComponentManager.Implementations;

	using NUnit.Framework;

	public sealed class ComponentManagerServiceTests
	{
		private ComponentManagerServiceTests()
		{
		}

		[SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable", Justification = "Test class")]
		public class GivenAComponentManagerService
		{
			private const string Webserver = ".\\Components\\WebServer";
			private ComponentManagerService _sut;

			[SetUp]
			public void Setup()
			{
				DirectoryExtensions.DeleteDirectory((string)Webserver);
				DirectoryExtensions.EnsureDirectoryExists((string)".\\Components");
				DirectoryExtensions.EnsureDirectoryExists((string)Webserver);

#if DEBUG
				const string Configuration = "Debug";
#else
				const string Configuration = "Release";
#endif

				var componentDir = string.Format(@"..\..\..\ComponentManager.WebServer\bin\{0}\", Configuration);

				foreach (var file in Directory.GetFiles(componentDir, "*.dll"))
				{
					var destFileName = Path.Combine(Path.GetFullPath(Webserver), Path.GetFileName(file));
					File.Copy(file, destFileName);
				}

				_sut = new ComponentManagerService();
			}

			[TearDown]
			public void Teardown()
			{
				_sut.Dispose();
			}

			[Test]
			public async Task WhenServiceStartedThenCanCallWebServer()
			{
				_sut.Run("manifest.json");

				var client = new WebClient();

				var response = await client.DownloadStringTaskAsync(new Uri("http://localhost:8888"));

				Assert.IsNotNullOrEmpty(response);

				_sut.Stop();
			}

			[Test(Description = "Must be run as administrator")]
			[Ignore]
			public async Task WhenWcfServiceStartedThenCanCallWcfServer()
			{
				_sut.Run("wcfmanifest.json");

				var client = new WebClient();

				var response = await client.DownloadStringTaskAsync(new Uri("http://localhost:8889/hello"));

				Assert.IsNotNullOrEmpty(response);

				_sut.Stop();
			}

			[Test(Description = "Must be run as administrator")]
			[Ignore]
			public void WhenWcfServiceStartedThenCanConnectAsClient()
			{
				_sut.Run("wcfmanifest.json");
				var binding = new BasicHttpBinding();
				var endpoint = new EndpointAddress("http://localhost:8889/hello");
				var channelFactory = new ChannelFactory<IHelloWorldService>(binding, endpoint);

				IHelloWorldService client = null;
				string response = null;
				try
				{
					client = channelFactory.CreateChannel();
					response = client.SayHello("Tester");
					((ICommunicationObject)client).Close();
				}
				catch
				{
					if (client != null)
					{
						((ICommunicationObject)client).Abort();
					}
				}

				Assert.IsNotNullOrEmpty(response);

				_sut.Stop();
			}

			[Test]
			public async Task WhenAddingFileInComponentFolderThenRestartsComponent()
			{
				_sut.Run("manifest.json");

				using (var writer = File.CreateText(Path.Combine(Webserver, "test.txt")))
				{
					writer.Write("test");
				}

				await Task.Delay(TimeSpan.FromSeconds(15));

				_sut.Stop();
			}
		}
	}
}