﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DirectoryExtensionsTests.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the DirectoryExtensionsTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Service.Tests
{
	using System.Security.Permissions;
	using System.Security.Policy;

	using ComponentManager.Implementations;

	using NUnit.Framework;

	public sealed class DirectoryExtensionsTests
	{
		private DirectoryExtensionsTests()
		{
		}

		public class GivenADirectoryExtensions
		{
			[Test]
			public void WhenCheckingInvalidPathAsChildThenReturnsFalse()
			{
				Assert.IsFalse(DirectoryExtensions.IsSubDirectoryOf((string)@"½", (string)null));
			}

			[Test]
			public void WhenCheckingValidSubdirectoryThenReturnsTrue()
			{
				Assert.IsTrue(DirectoryExtensions.IsSubDirectoryOf((string)@"C:\Windows\Microsoft.NET\assembly", (string)@"C:\Windows\"));
			}

			[Test]
			public void WhenCheckingValidDirectoryThatIsNotSubdirectoryThenReturnsFalse()
			{
				Assert.IsFalse(DirectoryExtensions.IsSubDirectoryOf((string)@"C:\Temp", (string)@"C:\Windows\"));
			}
		}
	}
}