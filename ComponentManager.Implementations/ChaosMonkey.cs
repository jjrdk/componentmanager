// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ChaosMonkey.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ChaosMonkey type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Implementations
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Reactive;
	using System.Reactive.Linq;
	using System.Security;

	using global::ComponentManager.Contracts;

	public class ChaosMonkey : IResilienceChallenger
	{
		private readonly TimeSpan _interval;

		private IDisposable _subscription;

		public ChaosMonkey(TimeSpan interval)
		{
			_interval = interval;
		}

		~ChaosMonkey()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		[SecuritySafeCritical]
		public void Start(IEnumerable<ComponentManifest> manifests)
		{
			_subscription = Observable.Timer(_interval, _interval)
				.Select(OnSelector)
				.Subscribe(new ManifestObserver(manifests.Select(x => x.Path)));
		}

		public void Stop()
		{
			_subscription.Dispose();
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				Stop();
			}
		}

		[SecuritySafeCritical]
		private Unit OnSelector(long x)
		{
			return Unit.Default;
		}

		private class ManifestObserver : IObserver<Unit>
		{
			private readonly string[] _paths;
			private readonly Random _random;

			public ManifestObserver(IEnumerable<string> paths)
			{
				_random = new Random(DateTime.UtcNow.Millisecond);
				_paths = paths.Where(x => x.IsDirectory()).ToArray();
			}

			/// <summary>
			/// Provides the observer with new data.
			/// </summary>
			/// <param name="value">The current notification information.</param>
			[SecuritySafeCritical]
			public void OnNext(Unit value)
			{
				var paths = Enumerable.Range(0, _random.Next(_paths.Length))
					.Select(x => _random.Next(_paths.Length))
					.Distinct()
					.Select(x => _paths[x])
					.Select(path => Path.Combine(path, Path.GetRandomFileName()))
					.ToArray();
				foreach (var fileName in paths)
				{
					Console.BackgroundColor = ConsoleColor.Red;
					Console.ForegroundColor = ConsoleColor.White;
					Console.WriteLine(fileName);
					Console.ResetColor();

					using (File.Create(fileName))
					{
					}

					File.Delete(fileName);
				}
			}

			/// <summary>
			/// Notifies the observer that the provider has experienced an error condition.
			/// </summary>
			/// <param name="error">An object that provides additional information about the error.</param>
			public void OnError(Exception error)
			{
			}

			/// <summary>
			/// Notifies the observer that the provider has finished sending push-based notifications.
			/// </summary>
			public void OnCompleted()
			{
			}
		}
	}
}