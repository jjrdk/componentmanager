// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageConnector.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the MessageConnector type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Implementations
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Reflection;
	using System.Security;
	using System.Threading.Tasks;

	using ComponentManager.Contracts;

	public class MessageConnector : IDisposable
	{
		private static readonly MethodInfo GetPayloadMethod;
		private readonly ILookup<string, Type> _subjectMap;
		private readonly Func<Type, IEnumerable<IPayloadHandler>> _handlerProvider;
		private readonly IDisposable _messageSubscription;

		static MessageConnector()
		{
			GetPayloadMethod = typeof(IEnvelope).GetMethods().First(x => x.Name == "GetPayload" && x.ContainsGenericParameters);
		}

		[SecuritySafeCritical]
		public MessageConnector(IObservable<IEnvelope> messageSource, ILookup<string, Type> subjectMap, Func<Type, IEnumerable<IPayloadHandler>> handlerProvider)
		{
			_subjectMap = subjectMap;
			_handlerProvider = handlerProvider;
			_messageSubscription = messageSource.Subscribe(OnEnvelope);
		}

		~MessageConnector()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				_messageSubscription.Dispose();
			}
		}

		private static object GetPayload(Type payloadType, IEnvelope envelope)
		{
			var genericMethod = GetPayloadMethod.MakeGenericMethod(payloadType);
			var payload = genericMethod.Invoke(envelope, null);

			return payload;
		}

		private void OnEnvelope(IEnvelope envelope)
		{
			var tasks = from type in _subjectMap[envelope.Subject]
						group type by type
						into g
						let payloadType = g.First()
						let payload = GetPayload(payloadType, envelope)
						from handler in _handlerProvider(payloadType)
						select handler.Handle(payload);
			Task.WaitAll(tasks.ToArray());
		}
	}
}