// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DirectoryExtensions.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the DirectoryExtensions type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Implementations
{
	using System.IO;

	internal static class DirectoryExtensions
	{
		public static bool IsDirectory(this string path)
		{
			return Directory.Exists(path);
		}

		public static bool IsFile(this string path)
		{
			return File.Exists(path);
		}

		public static void EnsureDirectoryExists(this string path)
		{
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
		}

		public static void DeleteDirectory(this string path)
		{
			if (Directory.Exists(path))
			{
				Directory.Delete(path, true);
			}
		}

		public static bool IsSubDirectoryOf(this string candidate, string other)
		{
			try
			{
				var candidateInfo = new DirectoryInfo(candidate);
				var otherInfo = new DirectoryInfo(other);

				while (candidateInfo.Parent != null)
				{
					if (candidateInfo.Parent.FullName == otherInfo.FullName.Trim('\\'))
					{
						return true;
					}

					candidateInfo = candidateInfo.Parent;
				}

				return false;
			}
			catch
			{
				return false;
			}
		}
	}
}