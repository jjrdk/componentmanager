﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   AssemblyInfo.cs
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
using System.Security;

[assembly: AssemblyTitle("ComponentManager.Implementations")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Reimers.dk")]
[assembly: AssemblyProduct("ComponentManager.Implementations")]
[assembly: AssemblyCopyright("Copyright © Reimers.dk 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6fb635c5-266e-4a48-a934-da8de470e55f")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AllowPartiallyTrustedCallers(PartialTrustVisibilityLevel = PartialTrustVisibilityLevel.NotVisibleByDefault)]
[assembly: InternalsVisibleTo("ComponentManager.Host, PublicKey=002400000480000094000000060200000024000052534131000400000100010047aec89d60cda6b0baa3f263034b4f3593e473bb6bd2bab7fe64d4d9ee6799657fea59df37e0e4f32cf68d78bd2cb3c64883cc5cd86586e0c43dad1a8fbb11d71187b00959aecd4e2c8227e7ead427e67de8cb397757cf04ce38d606551a53a92abdc1a8a2b61a613a918d8eb28b8ef1096f1c6d45e1eafa0d89b881d9e181a7")]
[assembly: InternalsVisibleTo("ComponentManager.Host.Tests, PublicKey=002400000480000094000000060200000024000052534131000400000100010047aec89d60cda6b0baa3f263034b4f3593e473bb6bd2bab7fe64d4d9ee6799657fea59df37e0e4f32cf68d78bd2cb3c64883cc5cd86586e0c43dad1a8fbb11d71187b00959aecd4e2c8227e7ead427e67de8cb397757cf04ce38d606551a53a92abdc1a8a2b61a613a918d8eb28b8ef1096f1c6d45e1eafa0d89b881d9e181a7")]
[assembly: InternalsVisibleTo("ComponentManager.Service.Tests, PublicKey=002400000480000094000000060200000024000052534131000400000100010047aec89d60cda6b0baa3f263034b4f3593e473bb6bd2bab7fe64d4d9ee6799657fea59df37e0e4f32cf68d78bd2cb3c64883cc5cd86586e0c43dad1a8fbb11d71187b00959aecd4e2c8227e7ead427e67de8cb397757cf04ce38d606551a53a92abdc1a8a2b61a613a918d8eb28b8ef1096f1c6d45e1eafa0d89b881d9e181a7")]
[assembly: InternalsVisibleTo("ComponentManager.Queueing.Tests, PublicKey=002400000480000094000000060200000024000052534131000400000100010047aec89d60cda6b0baa3f263034b4f3593e473bb6bd2bab7fe64d4d9ee6799657fea59df37e0e4f32cf68d78bd2cb3c64883cc5cd86586e0c43dad1a8fbb11d71187b00959aecd4e2c8227e7ead427e67de8cb397757cf04ce38d606551a53a92abdc1a8a2b61a613a918d8eb28b8ef1096f1c6d45e1eafa0d89b881d9e181a7")]
[assembly: InternalsVisibleTo("ComponentManager.Implementations.Tests, PublicKey=002400000480000094000000060200000024000052534131000400000100010047aec89d60cda6b0baa3f263034b4f3593e473bb6bd2bab7fe64d4d9ee6799657fea59df37e0e4f32cf68d78bd2cb3c64883cc5cd86586e0c43dad1a8fbb11d71187b00959aecd4e2c8227e7ead427e67de8cb397757cf04ce38d606551a53a92abdc1a8a2b61a613a918d8eb28b8ef1096f1c6d45e1eafa0d89b881d9e181a7")]