// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BackoffInvoker.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the BackoffInvoker type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Implementations
{
	using System;
	using System.Security;
	using System.Threading.Tasks;

	using global::ComponentManager.Contracts;

	using Polly;

	[SecuritySafeCritical]
	public sealed class BackoffInvoker : IDelegateInvoker
	{
		private readonly Policy _policy;

		[SecuritySafeCritical]
		public BackoffInvoker(uint maxRetries)
		{
			_policy = Policy.Handle<Exception>()
				.WaitAndRetryAsync(
					(int)maxRetries,
					retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
					(e, a) =>
					{
						Console.WriteLine(e.Message);
						Console.WriteLine(e.StackTrace);
					});
		}

		/// <summary>
		/// Executes the operation.
		/// </summary>
		/// <param name="action">Operation to execute.</param>
		[SecuritySafeCritical]
		public Task Execute(Action action)
		{
			Func<Task> task = () => Task.Factory.FromAsync(action.BeginInvoke, action.EndInvoke, null);

			return _policy.ExecuteAsync(task);
		}

		/// <summary>
		/// Executes the operation.
		/// </summary>
		/// <param name="action">Operation to execute.</param>
		/// <returns>Result of operation as an object.</returns>
		[SecuritySafeCritical]
		public Task<T> Execute<T>(Func<T> action)
		{
			Func<Task<T>> task = () => Task.Factory.FromAsync<T>(action.BeginInvoke, action.EndInvoke, null);

			return _policy.ExecuteAsync(task);
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
		}
	}
}