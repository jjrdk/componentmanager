// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebSocketMessageBrokerTests.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the WebSocketMessageBrokerTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Websockets.Tests
{
	using System;
	using System.Diagnostics;
	using System.Threading;

	using ComponentManager.Contracts;

	using Newtonsoft.Json;

	using NUnit.Framework;

	public sealed class WebSocketMessageBrokerTests
	{
		private WebSocketMessageBrokerTests()
		{
		}

		public class GivenAWebSocketMessageBroker
		{
			private WebSocketMessageBroker _sut;

			[SetUp]
			public void Setup()
			{
				_sut = new WebSocketMessageBroker();
			}

			[TearDown]
			public void Teardown()
			{
				_sut.Dispose();
			}

			[Test]
			public void SubscribersReceiveMessages()
			{
				const string Value = "Hello World";
				const string Endpoint = "ws://localhost:82/test";

				var waitHandle = new ManualResetEventSlim(false);
				var queue = new ObservableSocketServer(Endpoint);

				queue.Subscribe(new TestObserver<IEnvelope>(
					x =>
					{
						var container = x.GetPayload<MessageContainer>();
						if (container.Value == Value)
						{
							waitHandle.Set();
						}
					}));
				queue.Start();
				var subscriber = _sut.Subscribe(new SubscriptionRequest { Filter = "true", EndPoint = Endpoint });
				_sut.Start();

				var msg = new SocketEnvelope(
					"test",
					null,
					null,
					"testing",
					DateTime.UtcNow,
					TimeSpan.MaxValue,
					new MessageContainer { Value = Value });
				var json = JsonConvert.SerializeObject(msg);
				queue.Send(json);

				var result = waitHandle.Wait(TimeSpan.FromSeconds(Debugger.IsAttached ? 30 : 3));

				Assert.True(result);
				subscriber.Dispose();
			}
		}
	}
}