// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObservableSocketClientTests.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ObservableSocketClientTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Websockets.Tests
{
	using System;
	using System.Threading;

	using ComponentManager.Contracts;

	using Newtonsoft.Json;

	using NUnit.Framework;

	public sealed class ObservableSocketClientTests
	{
		private ObservableSocketClientTests()
		{
		}

		public class GivenAObservableSocketClient
		{
			private ObservableSocketClient _sut;
			private string _publishEndpoint;

			[SetUp]
			public void Setup()
			{
				_publishEndpoint = "ws://localhost:800/publish";
				_sut = new ObservableSocketClient(_publishEndpoint);
			}

			[TearDown]
			public void Teardown()
			{
				_sut.Dispose();
			}

			[Test]
			public void WhenSendingDataToQueueThenPublishesMessage()
			{
				var waitHandle = new ManualResetEventSlim(false);
				var server = new ObservableSocketServer(_publishEndpoint);
				_sut.Subscribe(new TestObserver<IEnvelope>(x => waitHandle.Set()));
				server.Start();
				_sut.Start();
				var envelope = new SocketEnvelope(
					Guid.NewGuid().ToString("N"),
					"None",
					"None",
					"None",
					DateTime.UtcNow,
					TimeSpan.MaxValue,
					"Hello World");
				var json = JsonConvert.SerializeObject(envelope);
				server.Send(json);

				var result = waitHandle.Wait(TimeSpan.FromSeconds(3));

				Assert.True(result);
			}
		}
	}
}