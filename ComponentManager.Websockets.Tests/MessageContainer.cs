// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageContainer.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the MessageContainer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Websockets.Tests
{
	public class MessageContainer
	{
		public string Value { get; set; }

		/// <summary>
		/// Returns a string that represents the current object.
		/// </summary>
		/// <returns>
		/// A string that represents the current object.
		/// </returns>
		public override string ToString()
		{
			return Value;
		}
	}
}