// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebSocketMoleTests.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the WebSocketMoleTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Websockets.Tests
{
	using System;
	using System.Threading;

	using ComponentManager.Contracts;

	using Newtonsoft.Json;

	using NUnit.Framework;

	using WebSocketSharp;

	public sealed class WebSocketMoleTests
	{
		private WebSocketMoleTests()
		{
		}

		public class GivenAWebSocketMole
		{
			private const string EndPoint = "ws://localhost:801/test";
			private WebSocketMole _sut;

			[SetUp]
			public void Setup()
			{
				_sut = new WebSocketMole(EndPoint);
				_sut.Start();
			}

			[TearDown]
			public void Teardown()
			{
				_sut.Dispose();
			}

			[Test]
			public void WhenSubscribingThenClientReceivesMessages()
			{
				var waitHandle = new ManualResetEventSlim(false);
				var client = new WebSocket(EndPoint);
				client.Connect();
				var subscription = _sut.Subscribe(new TestObserver<IEnvelope>(e => waitHandle.Set()));
				var message = new SocketEnvelope(string.Empty, string.Empty, string.Empty, string.Empty, DateTime.UtcNow, TimeSpan.MaxValue, "Test");
				var json = JsonConvert.SerializeObject(message);
				client.Send(json);

				var result = waitHandle.Wait(TimeSpan.FromSeconds(2));

				Assert.IsTrue(result);

				subscription.Dispose();
			}
		}
	}
}