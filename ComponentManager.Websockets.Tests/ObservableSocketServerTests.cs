﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObservableSocketServerTests.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ObservableSocketTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Websockets.Tests
{
	using System;
	using System.Threading;
	using ComponentManager.Contracts;
	using Newtonsoft.Json;
	using NUnit.Framework;
	using WebSocketSharp;

	public sealed class ObservableSocketServerTests
	{
		private ObservableSocketServerTests()
		{
		}

		public class GivenAObservableSocketServer
		{
			private ObservableSocketServer _sut;
			private string _publishEndpoint;

			[SetUp]
			public void Setup()
			{
				_publishEndpoint = "ws://localhost:800/publish";
				_sut = new ObservableSocketServer(_publishEndpoint);
				_sut.Start();
			}

			[TearDown]
			public void Teardown()
			{
				_sut.Dispose();
			}

			[Test]
			public void WhenDisposingThenDoesNotThrow()
			{
				Assert.DoesNotThrow(() => _sut.Dispose());
			}

			[Test]
			public void WhenSendingDataToQueueThenPublishesMessage()
			{
				var waitHandle = new ManualResetEventSlim(false);
				var client = new WebSocket(_publishEndpoint);
				_sut.Subscribe(new TestObserver<IEnvelope>(x => waitHandle.Set()));
				client.Connect();
				var envelope = new SocketEnvelope(
					Guid.NewGuid().ToString("N"),
					"None",
					"None",
					"None",
					DateTime.UtcNow,
					TimeSpan.MaxValue,
					"Hello World");
				var json = JsonConvert.SerializeObject(envelope);
				client.Send(json);

				var result = waitHandle.Wait(TimeSpan.FromSeconds(3));

				Assert.True(result);
			}
		}
	}
}
