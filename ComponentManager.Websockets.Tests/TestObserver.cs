// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestObserver.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the TestObserver type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Websockets.Tests
{
	using System;

	internal class TestObserver<T> : IObserver<T>
	{
		private readonly Action<T> _onNext;

		public TestObserver(Action<T> onNext)
		{
			_onNext = onNext;
		}

		/// <summary>
		/// Provides the observer with new data.
		/// </summary>
		/// <param name="value">The current notification information.</param>
		public void OnNext(T value)
		{
			_onNext(value);
		}

		/// <summary>
		/// Notifies the observer that the provider has experienced an error condition.
		/// </summary>
		/// <param name="error">An object that provides additional information about the error.</param>
		public void OnError(Exception error)
		{
		}

		/// <summary>
		/// Notifies the observer that the provider has finished sending push-based notifications.
		/// </summary>
		public void OnCompleted()
		{
		}
	}
}