﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Service
{
	using System.Diagnostics.CodeAnalysis;
	using System.ServiceProcess;

	internal static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[ExcludeFromCodeCoverage]
		private static void Main()
		{
			var services = new ServiceBase[] { new ComponentManagerService() };
			ServiceBase.Run(services);
		}
	}
}
