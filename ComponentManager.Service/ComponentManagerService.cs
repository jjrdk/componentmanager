﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentManagerService.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ComponentManagerService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Service
{
	using System;
	using System.Linq;
	using System.ServiceProcess;

	using ComponentManager.Host;
	using ComponentManager.Implementations;
	using ComponentManager.Queueing;

	public partial class ComponentManagerService : ServiceBase
	{
		private readonly ComponentManager _manager;

		public ComponentManagerService()
		{
			var queueFactory = new QueueFactory();
			var endpointProvider = new QueueNames("root");
			var broker = new QueuedMessageBroker(endpointProvider, queueFactory);
			_manager = new ComponentManager(broker, endpointProvider);
			InitializeComponent();
		}

		public void Run(params string[] args)
		{
			OnStart(args);
		}

		protected override void OnStart(string[] args)
		{
			_manager.Start(args.First(), () => new ChaosMonkey(TimeSpan.FromMinutes(2)));
		}

		protected override void OnStop()
		{
			_manager.Stop();
		}
	}
}
