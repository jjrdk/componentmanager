﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProjectInstaller.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ProjectInstaller type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Service
{
	using System.Collections;
	using System.ComponentModel;
	using System.Configuration.Install;
	using System.Diagnostics.CodeAnalysis;

	[ExcludeFromCodeCoverage]
	[RunInstaller(true)]
	public partial class ProjectInstaller : Installer
	{
		public ProjectInstaller()
		{
			InitializeComponent();
		}

		/// <summary>
		/// When overridden in a derived class, removes an installation.
		/// </summary>
		/// <param name="savedState">An <see cref="T:System.Collections.IDictionary"/> that contains the state of the computer after the installation was complete. </param><exception cref="T:System.ArgumentException">The saved-state <see cref="T:System.Collections.IDictionary"/> might have been corrupted. </exception><exception cref="T:System.Configuration.Install.InstallException">An exception occurred while uninstalling. This exception is ignored and the uninstall continues. However, the application might not be fully uninstalled after the uninstallation completes. </exception>
		public override void Uninstall(IDictionary savedState)
		{
			base.Uninstall(savedState);
		}
	}
}
