﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DirectoryExtensions.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the DirectoryExtensions type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Console
{
	using System.IO;

	internal static class DirectoryExtensions
	{
		public static void EnsureDirectoryExists(this string path)
		{
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
		}

		public static void DeleteDirectory(this string path)
		{
			if (Directory.Exists(path))
			{
				Directory.Delete(path, true);
			}
		}
	}
}