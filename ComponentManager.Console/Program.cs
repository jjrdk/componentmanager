﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Console
{
	using System;
	using System.IO;
	using System.Linq;

	using ComponentManager.Host;
	using ComponentManager.Implementations;
	using ComponentManager.Queueing;

	internal class Program
	{
		private const string WebServer = ".\\Components\\WebServer\\bin";
		private const string Client = ".\\Components\\QueueClient\\bin";
		private const string Publisher = ".\\Components\\QueuePublisher\\bin";

		private static void Main()
		{
			const string Prefix = "root";
			SetupFiles();
			var queueFactory = new QueueFactory();
			var provider = new QueueNames(Prefix);
			var broker = new QueuedMessageBroker(provider, queueFactory);
			var service = new ComponentManager(broker, provider);
			service.Start("manifest.json", () => new NoOpChallenger()); ////() => new ChaosMonkey(TimeSpan.FromMinutes(0.25)));
			var bridge = new ExternalProcessBridge(broker, provider, new MessageQueueMole(typeof(ExternalProcessBridge).FullName, provider));

			Console.WriteLine("Component Manager Running...");
			Console.WriteLine("Press Enter to Exit.");
			Console.ReadLine();

			bridge.Dispose();
			service.Stop();
		}

		private static void SetupFiles()
		{
			WebServer.DeleteDirectory();
			Client.DeleteDirectory();
			Publisher.DeleteDirectory();
			".\\Components".EnsureDirectoryExists();
			WebServer.EnsureDirectoryExists();
			Client.EnsureDirectoryExists();
			Publisher.EnsureDirectoryExists();

#if DEBUG
			const string Configuration = "Debug";
#else
			const string Configuration = "Release";
#endif

			var componentDir = string.Format(@"..\..\..\ComponentManager.Components\bin\{0}\", Configuration);

			var files = from file in Directory.GetFiles(componentDir, "*.dll")
				let fileName = Path.GetFileName(file)
				where !string.IsNullOrWhiteSpace(fileName)
				select new
				{
					Original = file,
					File = fileName
				};

			foreach (var file in files)
			{
				var publisherFileName = Path.Combine(Path.GetFullPath(Publisher), file.File);
				var clientFileName = Path.Combine(Path.GetFullPath(Client), file.File);

				File.Copy(file.Original, publisherFileName, true);
				File.Copy(file.Original, clientFileName, true);
			}

			CopyWebServer(Configuration);
		}

		private static void CopyWebServer(string configuration)
		{
			var componentDir = string.Format(@"..\..\..\ComponentManager.WebServer\bin\{0}\", configuration);

			var files = from file in Directory.GetFiles(componentDir, "*.dll")
						let fileName = Path.GetFileName(file)
						where !string.IsNullOrWhiteSpace(fileName)
						select new { Original = file, File = fileName };

			foreach (var file in files)
			{
				var webserverFileName = Path.Combine(Path.GetFullPath(WebServer), file.File);
				File.Copy(file.Original, webserverFileName, true);
			}
		}
	}
}
