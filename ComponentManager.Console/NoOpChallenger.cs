﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NoOpChallenger.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the NoOpChallenger type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Console
{
	using System.Collections.Generic;
	using ComponentManager.Contracts;

	internal class NoOpChallenger : IResilienceChallenger
	{
		public void Dispose()
		{
		}

		public void Start(IEnumerable<ComponentManifest> manifests)
		{
		}

		public void Stop()
		{
		}
	}
}