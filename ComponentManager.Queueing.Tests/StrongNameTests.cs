// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StrongNameTests.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the StrongNameTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Queueing.Tests
{
	using System;
	using System.Messaging;
	using System.Security;
	using System.Security.Permissions;
	using System.Security.Policy;

	using Newtonsoft.Json;

	using NUnit.Framework;

	public sealed class StrongNameTests
	{
		private StrongNameTests()
		{
		}

		public class GivenAStrongName
		{
			private StrongName _sut;

			[SetUp]
			public void Setup()
			{
				////var permissionSet = new PermissionSet(PermissionState.None);
				////permissionSet.AddPermission(new SecurityPermission(SecurityPermissionFlag.Execution | SecurityPermissionFlag.ControlPrincipal));
				////permissionSet.AddPermission(new MessageQueuePermission(PermissionState.Unrestricted));
				////Console.WriteLine(permissionSet.ToXml().ToString());

				////Evidence ev = new Evidence();
				////ev.AddHostEvidence(new Zone(SecurityZone.MyComputer));
				////var permissionSet = SecurityManager.GetStandardSandbox(ev);
				////Console.WriteLine(permissionSet.ToXml().ToString());

				var assemblyName = typeof(MessageQueueMole).Assembly.GetName();
				_sut = new StrongName(new StrongNamePublicKeyBlob(assemblyName.GetPublicKey()), assemblyName.Name, assemblyName.Version);
			}

			[TearDown]
			public void Teardown()
			{
			}

			[Test]
			public void CanSerializeStrongName()
			{
				var serialized = JsonConvert.SerializeObject(_sut);
				Console.WriteLine(_sut.PublicKey);
				Console.WriteLine(serialized);
				Assert.IsNotNullOrEmpty(serialized);
			}
		}
	}
}