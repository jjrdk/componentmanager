// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NoopChallenger.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the NoopChallenger type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Queueing.Tests
{
	using System.Collections.Generic;

	using ComponentManager.Contracts;
	using ComponentManager.Implementations;

	internal class NoopChallenger : IResilienceChallenger
	{
		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
		}

		public void Start(IEnumerable<ComponentManifest> manifests)
		{
		}

		public void Stop()
		{
		}
	}
}