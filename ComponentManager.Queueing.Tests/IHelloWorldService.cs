﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IHelloWorldService.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the IHelloWorldService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Queueing.Tests
{
	using System.ServiceModel;

	[ServiceContract]
	public interface IHelloWorldService
	{
		[OperationContract]
		string SayHello(string name);
	}
}