﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentManagerTests.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ComponentManagerServiceTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Queueing.Tests
{
	using System;
	using System.Diagnostics.CodeAnalysis;
	using System.IO;
	using System.Linq;
	using System.ServiceModel;
	using System.Threading.Tasks;

	using ComponentManager.Contracts;
	using ComponentManager.Host;
	using ComponentManager.Implementations;

	using NUnit.Framework;

	public sealed class ComponentManagerTests
	{
		private ComponentManagerTests()
		{
		}

		[SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable", Justification = "Test class")]
		public class GivenAComponentManager
		{
			private const string WcfServer = ".\\Components\\WcfServer";
			private const string Publisher = ".\\Components\\Publisher";
			private const string Client = ".\\Components\\Client";
			private ComponentManager _sut;

			[SetUp]
			public void Setup()
			{
				WcfServer.DeleteDirectory();
				Publisher.DeleteDirectory();
				Client.DeleteDirectory();
				".\\Components".EnsureDirectoryExists();
				WcfServer.EnsureDirectoryExists();
				Publisher.EnsureDirectoryExists();
				Client.EnsureDirectoryExists();

#if DEBUG
				const string Configuration = "Debug";
#else
				const string Configuration = "Release";
#endif

				var componentDir = string.Format(@"..\..\..\ComponentManager.Components\bin\{0}\", Configuration);

				foreach (var file in Directory.GetFiles(componentDir, "*.dll"))
				{
					var fileName = Path.GetFileName(file);

					var wcfserverFileName = Path.Combine(Path.GetFullPath(WcfServer), fileName);
					var publisherFileName = Path.Combine(Path.GetFullPath(Publisher), fileName);
					var clientFileName = Path.Combine(Path.GetFullPath(Client), fileName);

					File.Copy(file, wcfserverFileName);
					File.Copy(file, publisherFileName);
					File.Copy(file, clientFileName);
				}

				var queueFactory = new QueueFactory();
				var provider = new QueueNames("test");
				var broker = new QueuedMessageBroker(provider, queueFactory);

				_sut = new ComponentManager(broker, provider);
			}

			[TearDown]
			public void Teardown()
			{
				_sut.Dispose();
			}

			[Test]
			public async Task WhenServiceStartedThenPublishesMessages()
			{
				_sut.Start("manifest.json", () => new NoopChallenger());

				await Task.Delay(TimeSpan.FromSeconds(5));

				Assert.IsNotNullOrEmpty("a");

				_sut.Stop();
			}

			[Test]
			[Ignore("Must be run as administrator")]
			public void WhenWcfServiceStartedThenCanConnectAsClient()
			{
				_sut.Start("wcfmanifest.json", () => new NoopChallenger());
				var binding = new BasicHttpBinding();
				var endpoint = new EndpointAddress("http://localhost:8889/hello");
				var channelFactory = new ChannelFactory<IHelloWorldService>(binding, endpoint);

				IHelloWorldService client = null;
				string response = null;
				try
				{
					client = channelFactory.CreateChannel();
					response = client.SayHello("Tester");
					((ICommunicationObject)client).Close();
				}
				catch
				{
					if (client != null)
					{
						((ICommunicationObject)client).Abort();
					}
				}

				Assert.IsNotNullOrEmpty(response);

				_sut.Stop();
			}

			[Test(Description = "Must be run as administrator")]
			[Ignore]
			public async Task WhenWcfServiceStartedThenCanConnectWithManyClientProxies()
			{
				_sut.Start("wcfmanifest.json", () => new NoopChallenger());
				var binding = new BasicHttpBinding();
				var endpoint = new EndpointAddress("http://localhost:8889/hello");
				var channelFactory = new ChannelFactory<IHelloWorldService>(binding, endpoint);

				string[] results;
				IHelloWorldService client = null;
				try
				{
					client = channelFactory.CreateChannel();
					var proxy = new HelloWorldServiceProxy(client);

					var tasks = Enumerable.Range(0, 100)
						.Select(x => proxy.SayHello(x.ToString("X")))
						.ToArray();
					results = await Task.WhenAll(tasks);
					((ICommunicationObject)client).Close();
				}
				catch
				{
					results = new[] { string.Empty };
					if (client != null)
					{
						((ICommunicationObject)client).Abort();
					}
				}

				Assert.False(results.Any(string.IsNullOrWhiteSpace));

				_sut.Stop();
			}
		}

		private class HelloWorldServiceProxy : IDisposable
		{
			private readonly IHelloWorldService _innerService;
			private readonly IDelegateInvoker _buffer;

			public HelloWorldServiceProxy(IHelloWorldService innerService)
			{
				_innerService = innerService;
				_buffer = new CircuitBreakerInvoker(1, TimeSpan.FromSeconds(10));
			}

			~HelloWorldServiceProxy()
			{
				Dispose(false);
			}

			public async Task<string> SayHello(string name)
			{
				var result = await _buffer.Execute(() => _innerService.SayHello(name));

				return result;
			}

			public void Dispose()
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}

			protected virtual void Dispose(bool disposing)
			{
				if (disposing)
				{
					_buffer.Dispose();
				}

				// get rid of unmanaged resources
			}
		}
	}
}