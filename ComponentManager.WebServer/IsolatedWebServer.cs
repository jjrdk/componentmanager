﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IsolatedWebServer.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the IsolatedWebServer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Net;
using System.Text;
using System.Threading;
using ComponentManager.Contracts;

namespace ComponentManager.WebServer
{
	[Serializable]
	internal abstract class IsolatedWebServer : MoleComponentBase
	{
		[NonSerialized]
		private readonly HttpListener _server;
		private bool _running;

		public IsolatedWebServer(Mole mole, params Uri[] baseServiceAddresses)
			: base(TimeSpan.FromSeconds(15), mole, baseServiceAddresses)
		{
			_server = new HttpListener();
			_server.Prefixes.Add("http://localhost:8888/");
			_server.AuthenticationSchemes = AuthenticationSchemes.Anonymous;
		}

		public override void Run(ComponentParameters parameters)
		{
			base.Run(parameters);

			_server.Start();
			_running = true;
			SetInitializationComplete();

			while (_running)
			{
				Listen();
			}
		}

		public override void Stop()
		{
			_running = false;
		}

		protected override void Dispose(bool isDisposing)
		{
			base.Dispose(isDisposing);
			if (isDisposing)
			{
				_running = false;
				_server.Prefixes.Clear();
				_server.Stop();
				_server.Close();
			}
		}

		private void Listen()
		{
			try
			{
				var context = _server.GetContext();

				var msg = context.Request.HttpMethod + " " + context.Request.Url;

				Publish("QueueClient", "Request Received", msg);

				var principal = Thread.CurrentPrincipal;

				var sb = new StringBuilder();
				sb.Append("<html><body><h1>You requested:</h1>");
				sb.Append("<p>" + msg + "</p>");
				sb.Append("<p>" + (principal == null ? "Noone" : principal.Identity.Name) + "</p>");
				sb.Append("</body></html>");

				var b = Encoding.UTF8.GetBytes(sb.ToString());
				context.Response.ContentLength64 = b.Length;
				context.Response.OutputStream.Write(b, 0, b.Length);
				context.Response.OutputStream.Close();
			}
			catch (HttpListenerException)
			{
			}
		}
	}
}