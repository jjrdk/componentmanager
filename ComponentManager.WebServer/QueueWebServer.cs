﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueueWebServer.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the QueueWebServer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.WebServer
{
	using System;

	using ComponentManager.Contracts;
	using ComponentManager.Queueing;

	[Serializable]
	internal class QueueWebServer : IsolatedWebServer
	{
		public QueueWebServer()
			: base(new MessageQueueMole(typeof(QueueWebServer).FullName, new QueueNames(AppDomainSettings.EndpointPrefix)))
		{
		}
	}
}