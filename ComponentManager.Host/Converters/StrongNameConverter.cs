// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StrongNameConverter.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the StrongNameConverter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host.Converters
{
	using System;
	using System.Linq;
	using System.Security.Permissions;
	using System.Security.Policy;

	using Newtonsoft.Json;
	using Newtonsoft.Json.Linq;

	internal class StrongNameConverter : JsonConverter
	{
		/// <summary>
		/// Writes the JSON representation of the object.
		/// </summary>
		/// <param name="writer">The <see cref="T:Newtonsoft.Json.JsonWriter"/> to write to.</param><param name="value">The value.</param><param name="serializer">The calling serializer.</param>
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value == null)
			{
				writer.WriteNull();
				return;
			}

			var strongName = (StrongName)value;
			writer.WriteStartObject();
			writer.WritePropertyName("Name");
			writer.WriteValue(strongName.Name);
			writer.WritePropertyName("PublicKey");
			writer.WriteValue(strongName.PublicKey.ToString());
			writer.WritePropertyName("Version");
			writer.WriteValue(strongName.Version.ToString());
			writer.WriteEndObject();
		}

		/// <summary>
		/// Reads the JSON representation of the object.
		/// </summary>
		/// <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader"/> to read from.</param><param name="objectType">Type of the object.</param><param name="existingValue">The existing value of object being read.</param><param name="serializer">The calling serializer.</param>
		/// <returns>
		/// The object value.
		/// </returns>
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var value = JObject.Load(reader);
			var name = value.Value<string>("Name");
			var publicKey = value.Value<string>("PublicKey");
			var version = value.Value<string>("Version");

			var byteKey = StringToByteArray(publicKey);
			return new StrongName(new StrongNamePublicKeyBlob(byteKey), name, new Version(version));
		}

		/// <summary>
		/// Determines whether this instance can convert the specified object type.
		/// </summary>
		/// <param name="objectType">Type of the object.</param>
		/// <returns>
		/// <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
		/// </returns>
		public override bool CanConvert(Type objectType)
		{
			return typeof(StrongName).IsAssignableFrom(objectType);
		}

		private static byte[] StringToByteArray(string hex)
		{
			return Enumerable.Range(0, hex.Length)
							 .Where(x => x % 2 == 0)
							 .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
							 .ToArray();
		}
	}
}