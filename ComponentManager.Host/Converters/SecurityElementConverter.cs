// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SecurityElementConverter.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the SecurityElementConverter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host.Converters
{
	using System;
	using System.Security;

	using Newtonsoft.Json;

	internal class SecurityElementConverter : JsonConverter
	{
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			var se = value as SecurityElement;
			if (se == null)
			{
				writer.WriteNull();
			}
			else
			{
				writer.WriteValue(se.ToString());
			}
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var input = (string)reader.Value;
			return SecurityElement.FromString(input);
		}

		public override bool CanConvert(Type objectType)
		{
			return typeof(SecurityElement).IsAssignableFrom(objectType);
		}
	}
}