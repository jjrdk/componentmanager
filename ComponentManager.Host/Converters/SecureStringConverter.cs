// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SecureStringConverter.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the SecureStringConverter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host.Converters
{
	using System;
	using System.Security;

	using Newtonsoft.Json;

	internal class SecureStringConverter : JsonConverter
	{
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			writer.WriteValue("Secure String");
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var input = (string)reader.Value;
			var securestring = new SecureString();
			if (string.IsNullOrEmpty(input))
			{
				return securestring;
			}

			foreach (var character in input)
			{
				securestring.AppendChar(character);
			}

			return securestring;
		}

		public override bool CanConvert(Type objectType)
		{
			return typeof(SecureString).IsAssignableFrom(objectType);
		}
	}
}