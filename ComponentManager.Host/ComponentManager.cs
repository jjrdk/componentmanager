// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentManager.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ComponentManager type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host
{
	using System;
	using System.IO;
	using System.Linq;
	using System.Security;

	using global::ComponentManager.Contracts;
	using global::ComponentManager.Implementations;

	public class ComponentManager : IDisposable
	{
		private readonly IMessageBroker _broker;
		private readonly IEndPointProvider _endPointProvider;
		private readonly ManifestLoader _manifestLoader;
		private ComponentHost[] _hosts;
		private IResilienceChallenger _challenger;

		public ComponentManager(IMessageBroker messageBroker, IEndPointProvider endPointProvider)
		{
			var parentDir = Path.GetFullPath(".\\");
			var componentDir = Path.Combine(parentDir, "Components");
			if (!componentDir.IsDirectory())
			{
				throw new DirectoryNotFoundException("Could not find " + componentDir);
			}

			_broker = messageBroker;
			_endPointProvider = endPointProvider;
			_manifestLoader = new ManifestLoader(componentDir);
		}

		~ComponentManager()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		[SecuritySafeCritical]
		public void Start(string manifestPath, Func<IResilienceChallenger> resilienceChallengerFactory)
		{
			if (!manifestPath.IsFile())
			{
				throw new ArgumentException("manifestPath must point to file.");
			}

			if (resilienceChallengerFactory == null)
			{
				throw new ArgumentNullException("resilienceChallengerFactory");
			}

			AppDomain.MonitoringIsEnabled = true;
			var manifests = _manifestLoader.Load(manifestPath).ToArray();

			_hosts = manifests
				.Select(manifest => new ComponentHost(manifest, _broker, _endPointProvider))
				.ToArray();

			var allStarted = _hosts.Aggregate(
				true,
				(b, h) =>
				{
					try
					{
						return b && h.Start();
					}
					catch (Exception e)
					{
						_broker.Send(new InfrastructureEnvelope("Failed to start component", new LogMessage(0, e.Message)));
						return false;
					}
				});

			if (allStarted)
			{
				_broker.Start();
				_challenger = resilienceChallengerFactory.Invoke();
				_challenger.Start(manifests);
			}
			else
			{
				Stop();
			}
		}

		public void Stop()
		{
			foreach (var host in _hosts)
			{
				host.Dispose();
			}
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				Stop();
				_broker.Dispose();
			}
		}
	}
}