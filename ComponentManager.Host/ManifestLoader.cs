// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ManifestLoader.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ManifestLoader type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host
{
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;

	using global::ComponentManager.Contracts;
	using global::ComponentManager.Host.Converters;
	using global::ComponentManager.Implementations;

	using Newtonsoft.Json;

	internal class ManifestLoader
	{
		private readonly string _appDir;
		private readonly JsonSerializerSettings _settings;
		
		public ManifestLoader(string appDir)
		{
			_appDir = appDir;
			_settings = new JsonSerializerSettings();
			_settings.Converters.Add(new SecureStringConverter());
			_settings.Converters.Add(new SecurityElementConverter());
			_settings.Converters.Add(new StrongNameConverter());
		}

		public IEnumerable<ComponentManifest> Load(string path)
		{
			var manifestFile = Path.GetFullPath(path);
			var manifests = JsonConvert.DeserializeObject<ComponentManifest[]>(File.ReadAllText(manifestFile), _settings);

			foreach (var host in manifests)
			{
				host.UpdatePath(_appDir);
			}

			return !manifests.All(x => x.Path.IsDirectory() && x.Path.IsSubDirectoryOf(_appDir))
					   ? Enumerable.Empty<ComponentManifest>()
					   : manifests;
		}
	}
}