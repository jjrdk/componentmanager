﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourceMonitor.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ResourceMonitor type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host
{
	using System;
	using System.Reactive.Linq;
	using System.Reactive.Subjects;

	internal class ResourceMonitor : IObservable<ResourceStatus>, IDisposable
	{
		private readonly Subject<ResourceStatus> _resourceSubject = new Subject<ResourceStatus>();
		private readonly IDisposable _resourceSubscription;

		public ResourceMonitor(AppDomain domain, TimeSpan resourceStatusFrequency)
		{
			_resourceSubscription = Observable.Timer(resourceStatusFrequency, resourceStatusFrequency)
				.Select(x => new ResourceStatus(domain.FriendlyName, domain.MonitoringTotalProcessorTime, domain.MonitoringSurvivedMemorySize))
				.Subscribe(UpdateResourceStatus);
		}

		~ResourceMonitor()
		{
			Dispose(false);
		}

		/// <summary>
		/// Notifies the provider that an observer is to receive notifications.
		/// </summary>
		/// <returns>
		/// A reference to an interface that allows observers to stop receiving notifications before the provider has finished sending them.
		/// </returns>
		/// <param name="observer">The object that is to receive notifications.</param>
		public IDisposable Subscribe(IObserver<ResourceStatus> observer)
		{
			return _resourceSubject.Subscribe(observer);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				_resourceSubject.OnCompleted();
				_resourceSubscription.Dispose();
				_resourceSubject.Dispose();
			}

			// get rid of unmanaged resources
		}

		private void UpdateResourceStatus(ResourceStatus status)
		{
			_resourceSubject.OnNext(status);
		}
	}
}