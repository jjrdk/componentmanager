﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IsolationStartupResult.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the IsolationStartupResult type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using ComponentManager.Contracts;

namespace ComponentManager.Host
{
	internal struct IsolationStartupResult
	{
		private readonly bool _started;
		private readonly SubscriptionRequest _subscription;

		public IsolationStartupResult(bool started, string filter, string receiveEndPoint)
		{
			_started = started;
			_subscription = !string.IsNullOrWhiteSpace(filter) && !string.IsNullOrWhiteSpace(receiveEndPoint)
								? new SubscriptionRequest { EndPoint = receiveEndPoint, Filter = filter }
								: null;
		}

		public SubscriptionRequest Subscription
		{
			get { return _subscription; }
		}

		public bool Started
		{
			get
			{
				return _started;
			}
		}
	}
}