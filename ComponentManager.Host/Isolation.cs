﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Isolation.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the Isolation type.
//   Code inspired from http://www.superstarcoders.com/blogs/posts/executing-code-in-a-separate-application-domain-using-c-sharp.aspx
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host
{
	using System;
	using System.IO;
	using System.Linq;
	using System.Runtime.InteropServices;
	using System.Security;
	using System.Security.Permissions;
	using System.Security.Policy;
	using System.Threading;
	using System.Threading.Tasks;

	using global::ComponentManager.Contracts;

	[SecuritySafeCritical]
	internal class Isolation : IObservable<ResourceStatus>, IDisposable
	{
		private readonly ResourceMonitor _monitor;
		private readonly IComponent _value;
		private readonly Lazy<Task> _runTask;
		private readonly string _waitHandleName = Guid.NewGuid().ToString("N");
		private readonly EventWaitHandle _waitHandle;
		private readonly ComponentManifest _manifest;
		private readonly IEndPointProvider _endPointProvider;
		private AppDomain _domain;
		private bool _faulted;
		private bool _disposed;

		[SecuritySafeCritical]
		public Isolation(ComponentManifest manifest, TimeSpan resourceStatusFrequency, IEndPointProvider endPointProvider)
		{
			var appCache = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("B"));
			_manifest = manifest;
			_endPointProvider = endPointProvider;
			var domainArgs = new[] { endPointProvider.Prefix }.Concat(_endPointProvider.GetBaseServiceAddresses(manifest.Name)).ToArray();

			var setup = new AppDomainSetup
			{
				ApplicationBase = manifest.Path,
				AppDomainInitializer = Contracts.AppDomainInitializer.Initialize,
				AppDomainInitializerArguments = domainArgs,
				DisallowApplicationBaseProbing = false,
				PrivateBinPath = "bin",
				ShadowCopyFiles = "true",
				CachePath = appCache,
				DisallowBindingRedirects = false,
				DisallowCodeDownload = false
			};

			var evidence = new Evidence(
				new EvidenceBase[] { new Url(Path.Combine(appCache, manifest.Assembly)), new Zone(SecurityZone.MyComputer) },
				new EvidenceBase[0]);
			var permissions = new PermissionSet(PermissionState.None);
			permissions.FromXml(manifest.Permissions);
			permissions.AddPermission(new ReflectionPermission(PermissionState.Unrestricted));
			_domain = AppDomain.CreateDomain(_manifest.Name, evidence, setup, permissions, manifest.StrongNames);
			_monitor = new ResourceMonitor(_domain, resourceStatusFrequency);

			_waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset, _waitHandleName);
			var o = _domain.CreateInstanceAndUnwrap(manifest.Assembly, manifest.Type);
			_value = o as IComponent;

			_runTask = new Lazy<Task>(() => Task.Factory.StartNew(InnerRun, TaskCreationOptions.LongRunning));
		}

		~Isolation()
		{
			Dispose(false);
		}

		public event EventHandler<IsolationStoppedEventArgs> Stopped;

		public IsolationStartupResult Run()
		{
			var messageComponent = _value as IMessageComponent;
			var filter = messageComponent == null ? null : messageComponent.Filter;
			var task = _runTask.Value;
			var initializationTimeout = _value.InitializationTimeout;
			return task.Wait(0) || _waitHandle.WaitOne(initializationTimeout)
				? new IsolationStartupResult(true, filter, _endPointProvider.GetReceiveEndPoint(_manifest.Type))
				: new IsolationStartupResult(false, null, null);
		}

		/// <summary>
		/// Notifies the provider that an observer is to receive notifications.
		/// </summary>
		/// <returns>
		/// A reference to an interface that allows observers to stop receiving notifications before the provider has finished sending them.
		/// </returns>
		/// <param name="observer">The object that is to receive notifications.</param>
		[SecuritySafeCritical]
		public IDisposable Subscribe(IObserver<ResourceStatus> observer)
		{
			return _monitor.Subscribe(observer);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool isDisposing)
		{
			if (_disposed)
			{
				return;
			}

			_disposed = true;
			if (isDisposing)
			{
				if (_waitHandle != null)
				{
					_waitHandle.Dispose();
				}

				if (_domain != null)
				{
					_monitor.Dispose();
					_value.Dispose();
					try
					{
						AppDomain.Unload(_domain);
					}
					catch (CannotUnloadAppDomainException)
					{
					}

					_domain = null;
				}

				if (_runTask.IsValueCreated)
				{
					try
					{
						_runTask.Value.Dispose();
					}
					catch
					{
					}
				}
			}

			OnStopped();
		}

		private void OnStopped()
		{
			if (Stopped != null)
			{
				Stopped(this, new IsolationStoppedEventArgs(_faulted));
			}
		}

		private void InnerRun()
		{
			try
			{
				var pointer = _manifest.Password == null || _manifest.Password.Length == 0
								  ? IntPtr.Zero
								  : Marshal.SecureStringToGlobalAllocUnicode(_manifest.Password);
				var parameters = new ComponentParameters(
					_endPointProvider.GetPublishEndPoint(),
					_endPointProvider.GetReceiveEndPoint(_manifest.Type),
					_waitHandleName,
					_manifest.Username,
					_manifest.Domain,
					pointer);
				_value.Run(parameters);
			}
			catch (AppDomainUnloadedException)
			{
			}
			catch (Exception)
			{
				_faulted = true;
			}
		}
	}
}