// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExternalProcessBridge.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ExternalProcessBridge type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host
{
	using System;
	using global::ComponentManager.Contracts;

	public class ExternalProcessBridge : IDisposable
	{
		private readonly Mole _mole;
		private readonly IDisposable _subscription;

		public ExternalProcessBridge(IMessageBroker messageBroker, IEndPointProvider endPointProvider, Mole mole)
		{
			_mole = mole;
			_mole.Subscribe(new MessageObserver(messageBroker));
			_mole.Start();
			_subscription = messageBroker.Subscribe(new SubscriptionRequest
			{
				EndPoint = endPointProvider.GetReceiveEndPoint(typeof(ExternalProcessBridge).FullName),
				Filter = "Subject eq 'Subscription'"
			});
		}

		~ExternalProcessBridge()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool isDisposing)
		{
			if (isDisposing)
			{
				_subscription.Dispose();
				_mole.Dispose();
			}
		}

		private class MessageObserver : IObserver<IEnvelope>
		{
			private readonly IMessageBroker _messageBroker;

			public MessageObserver(IMessageBroker messageBroker)
			{
				_messageBroker = messageBroker;
			}

			public void OnNext(IEnvelope value)
			{
				var subscriptionRequest = value.GetPayload<SubscriptionRequest>();
				_messageBroker.Subscribe(subscriptionRequest);
			}

			public void OnError(Exception error)
			{
			}

			public void OnCompleted()
			{
			}
		}
	}
}