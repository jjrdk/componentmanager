// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourceStatus.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ResourceStatus type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host
{
	using System;

	[Serializable]
	internal class ResourceStatus
	{
		public ResourceStatus(string name, TimeSpan processingTime, long allocatedMemory)
		{
			Name = name;
			ProcessingTime = processingTime;
			AllocatedMemory = allocatedMemory;
		}

		public string Name { get; private set; }

		public TimeSpan ProcessingTime { get; private set; }

		public long AllocatedMemory { get; private set; }
	}
}