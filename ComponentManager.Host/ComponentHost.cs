// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentHost.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ComponentHost type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host
{
	using System;
	using System.Security;

	using global::ComponentManager.Contracts;

	internal sealed class ComponentHost : IDisposable
	{
		private const int MaxFaults = 3;
		private const string ResourceTrackingSubject = "Resource Tracking";
		private const string InfrastructureSubject = "Infrastructure";
		private readonly ComponentManifest _manifest;
		private readonly IMessageBroker _broker;
		private readonly IEndPointProvider _endPointProvider;
		private IDisposable _changeSubscription;
		private Isolation _isolation;
		private int _faultCount;
		private ComponentFileSystemMonitor _fileSystemMonitor;
		private IDisposable _messageSubscription;

		private bool _disposed;
		private IDisposable _resourceSubscription;

		public ComponentHost(ComponentManifest manifest, IMessageBroker broker, IEndPointProvider endPointProvider)
		{
			_manifest = manifest;
			_broker = broker;
			_endPointProvider = endPointProvider;
		}

		~ComponentHost()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		[SecuritySafeCritical]
		public bool Start()
		{
			if (_fileSystemMonitor == null)
			{
				_fileSystemMonitor = new ComponentFileSystemMonitor(_manifest.Path, TimeSpan.FromSeconds(30));
				_changeSubscription = _fileSystemMonitor.Subscribe(x => RestartDomain(false));
			}

			try
			{
				_isolation = new Isolation(_manifest, TimeSpan.FromSeconds(30), _endPointProvider);
				_isolation.Stopped += OnIsolationStopped;
				_resourceSubscription = _isolation.Subscribe(PublishResourceStatus, PublishResourceStatusComplete);
				var result = _isolation.Run();
				if (!result.Started)
				{
					_broker.Send(new InfrastructureEnvelope(InfrastructureSubject, new LogMessage(0, "Failed to start " + _manifest.Name)));
					return false;
				}

				var existingSubscription = _messageSubscription;
				if (result.Subscription != null)
				{
					_messageSubscription = _broker.Subscribe(result.Subscription);

					LogInfrastructureEvent("Subscribed to messages from " + result.Subscription.EndPoint);
				}

				if (existingSubscription != null)
				{
					existingSubscription.Dispose();
				}

				_broker.Send(new InfrastructureEnvelope(InfrastructureSubject, result));

				return result.Started;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				Console.WriteLine(ex.StackTrace);
				return false;
			}
		}

		private void Dispose(bool disposing)
		{
			if (_disposed)
			{
				return;
			}

			_disposed = true;
			if (disposing)
			{
				CleanupSubscriptions();
				if (_fileSystemMonitor != null)
				{
					_fileSystemMonitor.Dispose();
				}

				CleanupIsolation();
			}
		}

		private void PublishResourceStatus(ResourceStatus status)
		{
			_broker.Send(new InfrastructureEnvelope(ResourceTrackingSubject, status));
		}

		private void PublishResourceStatusComplete()
		{
			_broker.Send(new InfrastructureEnvelope(ResourceTrackingSubject, new ResourceStatus(_manifest.Name, TimeSpan.MinValue, 0)));
		}

		private void CleanupSubscriptions()
		{
			if (_messageSubscription != null)
			{
				_messageSubscription.Dispose();
			}

			if (_changeSubscription != null)
			{
				_changeSubscription.Dispose();
			}
		}

		private void OnIsolationStopped(object sender, IsolationStoppedEventArgs args)
		{
			if (args.Faulted)
			{
				RestartDomain(true);
			}
			else
			{
				Dispose();
			}
		}

		private void RestartDomain(bool isFault)
		{
			LogInfrastructureEvent("Restarting " + _manifest.Name);

			CleanupIsolation();

			if (!isFault || ++_faultCount <= MaxFaults)
			{
				Start();
			}
			else
			{
				LogInfrastructureEvent("Too many errors in " + _manifest.Type);
			}
		}

		private void LogInfrastructureEvent(string message)
		{
			_broker.Send(new InfrastructureEnvelope(InfrastructureSubject, new LogMessage(0, message)));
		}

		private void CleanupIsolation()
		{
			if (_isolation != null)
			{
				_resourceSubscription.Dispose();
				_isolation.Stopped -= OnIsolationStopped;
				_isolation.Dispose();
				_isolation = null;
			}
		}
	}
}