﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IsolationStoppedEventArgs.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the IsolationStoppedEventArgs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host
{
	using System;

	internal class IsolationStoppedEventArgs : EventArgs
	{
		public IsolationStoppedEventArgs(bool faulted)
		{
			Faulted = faulted;
		}

		public bool Faulted { get; private set; }
	}
}