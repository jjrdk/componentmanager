// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InfrastructureEnvelope.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the InfrastructureEnvelope type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host
{
	using System;
	using System.Security;

	using global::ComponentManager.Contracts;

	[SecuritySafeCritical]
	internal class InfrastructureEnvelope : IEnvelope
	{
		private readonly object _payload;

		public InfrastructureEnvelope(string subject, object payload)
		{
			_payload = payload;
			ID = Guid.NewGuid().ToString("N");
			Target = "Infrastructure";
			Subject = subject;
			Sent = DateTime.UtcNow;
			MaxAge = TimeSpan.MaxValue;
			_payload = payload;
		}

		public string ID { get; private set; }

		public string Target { get; private set; }

		public string Subject { get; private set; }

		public string CorrelationID { get; private set; }

		public DateTime Sent { get; private set; }

		public TimeSpan MaxAge { get; private set; }

		public T GetPayload<T>()
		{
			return (T)_payload;
		}

		public object GetPayload()
		{
			return _payload;
		}
	}
}