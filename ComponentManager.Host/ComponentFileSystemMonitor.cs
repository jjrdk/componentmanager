// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentFileSystemMonitor.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ComponentFileSystemMonitor type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host
{
	using System;
	using System.IO;
	using System.Linq;
	using System.Reactive;
	using System.Reactive.Concurrency;
	using System.Reactive.Linq;
	using System.Security;

	[SecuritySafeCritical]
	internal sealed class ComponentFileSystemMonitor : IObservable<Unit>, IDisposable
	{
		private readonly FileSystemWatcher _watcher;
		private readonly IObservable<Unit> _appSource;

		[SecuritySafeCritical]
		public ComponentFileSystemMonitor(string assemblyDir, TimeSpan delay)
		{
			const NotifyFilters NotifyFilters = NotifyFilters.Attributes | NotifyFilters.CreationTime | NotifyFilters.DirectoryName | NotifyFilters.FileName | NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.Security | NotifyFilters.Size;
			_watcher = new FileSystemWatcher(assemblyDir, "*.*")
						   {
							   IncludeSubdirectories = true,
							   NotifyFilter = NotifyFilters,
						   };

			var changeSource = Observable.FromEventPattern<FileSystemEventHandler, FileSystemEventArgs>(
				h => _watcher.Changed += h,
				h => _watcher.Changed -= h);
			var deleteSource = Observable.FromEventPattern<FileSystemEventHandler, FileSystemEventArgs>(
				h => _watcher.Deleted += h,
				h => _watcher.Deleted -= h);
			var createSource = Observable.FromEventPattern<FileSystemEventHandler, FileSystemEventArgs>(
				h => _watcher.Created += h,
				h => _watcher.Created -= h);
			var renameSource = Observable.FromEventPattern<RenamedEventHandler, FileSystemEventArgs>(
				h => _watcher.Renamed += h,
				h => _watcher.Renamed -= h);

			_appSource = changeSource
				.Merge(deleteSource)
				.Merge(createSource)
				.Merge(renameSource)
				.Select(x => x.EventArgs.ChangeType)
				.Buffer(delay)
				.Where(x => x.Any())
				.Select(x => Unit.Default)
				.ObserveOn(TaskPoolScheduler.Default);

			_watcher.EnableRaisingEvents = true;
		}

		[SecuritySafeCritical]
		public IDisposable Subscribe(IObserver<Unit> observer)
		{
			return _appSource.Subscribe(observer);
		}

		public void Dispose()
		{
			_watcher.Dispose();
		}
	}
}
