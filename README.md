# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Version : 0.1 - beta

Currently it is still a quick proof of concept about my idea for a component hosting infrastructure which supports dynamic upgrading a la ASP.NET applications. It runs as a Windows Service and has a root folder where the components are located. Each component is loaded into a ComponentHost which creates a file system monitor to watch for changes to the component. It also loads up the component in a separate AppDomain. The AppDomain is monitored for exceptions and restarted if necessary.

The application is defined in a manifest file, which is essentially a json file with an array of component definitions, like so:

[
                {
                                "Name" : "WebServer",
                                "Path" : "WebServer",
                                "Type" : "ComponentManager.Components.IsolatedWebServer",
                                "Assembly" : "ComponentManager.Components"
                }
]

See the [sample manifest file](https://bitbucket.org/jjrdk/componentmanager/src/9dcda836c74b0759997eaf80f5ec5c1370ce7e3e/ComponentManager.Service/manifest.json?at=master).

That is a test example which loads up a dumb web server.

There are a couple of things to notice. When starting up the service expects a ‘Components’ folder to exist in the service root (this will change). Inside the Components folder it expects to find the components as defined by the Path property. If the Path property points to something that is not a child of the Components folder it will not start up. It also expects the component entry point definition (Assembly + Type) to remain constant across versions, and each component entry point, i.e. the defined type, must inherit from IComponent and MarshalByRefObject. This is why I created the ComponentBase class to simplify creation.

Each IComponent is loaded in the order of the manifest. During the initial loading, the ComponentManagerService waits for each component to be initialized before loading the next. The wait period is defined by the InitializationTimeout of the component. If it does not start up in the specified time, it is considered failed and the loading will stop and so will the service. After the initial load, if a component fails or is updated, it will be restarted, but the other components will continue to run. So they will obviously need to be built to handle faults in other components.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

If you have comments, send me a message through BitBucket.

Pull requests are very welcome.