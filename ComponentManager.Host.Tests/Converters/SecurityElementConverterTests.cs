﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SecurityElementConverterTests.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the SecurityElementConverterTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host.Tests.Converters
{
	using System.IO;
	using System.Security;
	using System.Security.Permissions;
	using System.Text;

	using global::ComponentManager.Host.Converters;

	using Newtonsoft.Json;

	using NUnit.Framework;

	public sealed class SecurityElementConverterTests
	{
		private SecurityElementConverterTests()
		{
		}

		public class GivenASecurityElementConverter
		{
			private SecurityElementConverter _sut;

			[SetUp]
			public void Setup()
			{
				_sut = new SecurityElementConverter();
			}

			[TearDown]
			public void Teardown()
			{
			}

			[Test]
			public void WhenSerializingNullThenWritesNullValue()
			{
				var writer = new StringBuilder();
				_sut.WriteJson(new JsonTextWriter(new StringWriter(writer)), null, new JsonSerializer());

				Assert.AreEqual("null", writer.ToString());
			}

			[Test]
			public void WhenWritingInstanceThenWriteToStringValue()
			{
				var set = new PermissionSet(PermissionState.Unrestricted);
				var se = set.ToXml();
				var writer = new StringBuilder();
				_sut.WriteJson(new JsonTextWriter(new StringWriter(writer)), se, new JsonSerializer());

				Assert.AreEqual("\"<PermissionSet class=\\\"System.Security.PermissionSet\\\"\\r\\nversion=\\\"1\\\"\\r\\nUnrestricted=\\\"true\\\"/>\\r\\n\"", writer.ToString());
			}

			[Test]
			public void CanConvertFromStringInput()
			{
				var input = "{\"se\":\"<PermissionSet class=\\\"System.Security.PermissionSet\\\"\\r\\nversion=\\\"1\\\"\\r\\nUnrestricted=\\\"true\\\"/>\\r\\n\"}";
				var jsonTextReader = new JsonTextReader(new StringReader(input));
				jsonTextReader.Read();
				jsonTextReader.Read();
				jsonTextReader.Read();
				var result = _sut.ReadJson(jsonTextReader, typeof(SecurityElement), null, new JsonSerializer());

				Assert.IsInstanceOf<SecurityElement>(result);
			}
		}
	}
}
