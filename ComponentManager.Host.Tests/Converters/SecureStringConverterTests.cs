﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SecureStringConverterTests.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the SecureStringConverterTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host.Tests.Converters
{
	using System.IO;
	using System.Security;
	using System.Text;

	using global::ComponentManager.Host.Converters;

	using Newtonsoft.Json;

	using NUnit.Framework;

	public sealed class SecureStringConverterTests
	{
		private SecureStringConverterTests()
		{
		}

		public class GivenASecureStringConverter
		{
			private SecureStringConverter _sut;

			[SetUp]
			public void Setup()
			{
				_sut = new SecureStringConverter();
			}

			[TearDown]
			public void Teardown()
			{
			}

			[Test]
			public void WhenSerializingNullThenWritesDefaultValue()
			{
				var writer = new StringBuilder();
				_sut.WriteJson(new JsonTextWriter(new StringWriter(writer)), null, new JsonSerializer());

				Assert.AreEqual("\"Secure String\"", writer.ToString());
			}

			[Test]
			public void WhenWritingInstanceThenWriteToStringValue()
			{
				var value = "Text";
				var secureString = new SecureString();
				foreach (var character in value)
				{
					secureString.AppendChar(character);
				}

				var writer = new StringBuilder();
				_sut.WriteJson(new JsonTextWriter(new StringWriter(writer)), secureString, new JsonSerializer());

				Assert.AreEqual("\"Secure String\"", writer.ToString());
			}

			[Test]
			public void CanConvertFromStringInput()
			{
				var input = "{\"Name\":\"test\"}";
				var jsonTextReader = new JsonTextReader(new StringReader(input));
				jsonTextReader.Read();
				var result = _sut.ReadJson(jsonTextReader, typeof(SecureString), null, new JsonSerializer());

				Assert.IsInstanceOf<SecureString>(result);
			}
		}
	}
}