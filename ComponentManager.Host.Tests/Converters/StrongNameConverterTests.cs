﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StrongNameConverterTests.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the StrongNameConverterTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host.Tests.Converters
{
	using System;
	using System.IO;
	using System.Security.Permissions;
	using System.Security.Policy;
	using System.Text;

	using global::ComponentManager.Host.Converters;

	using Newtonsoft.Json;

	using NUnit.Framework;

	public sealed class StrongNameConverterTests
	{
		private StrongNameConverterTests()
		{
		}

		public class GivenAStrongNameConverter
		{
			private StrongNameConverter _sut;

			[SetUp]
			public void Setup()
			{
				_sut = new StrongNameConverter();
			}

			[TearDown]
			public void Teardown()
			{
			}

			[Test]
			public void WhenSerializingNullThenWritesNullValue()
			{
				var writer = new StringBuilder();
				_sut.WriteJson(new JsonTextWriter(new StringWriter(writer)), null, new JsonSerializer());

				Assert.AreEqual("null", writer.ToString());
			}

			[Test]
			public void WhenWritingInstanceThenWriteToStringValue()
			{
				var strongName = new StrongName(new StrongNamePublicKeyBlob(new byte[] { 0 }), "test", new Version());
				var writer = new StringBuilder();
				_sut.WriteJson(new JsonTextWriter(new StringWriter(writer)), strongName, new JsonSerializer());
				
				Assert.AreEqual("{\"Name\":\"test\",\"PublicKey\":\"00\",\"Version\":\"0.0\"}", writer.ToString());
			}

			[Test]
			public void CanConvertFromStringInput()
			{
				var input = "{\"Name\":\"test\",\"PublicKey\":\"00\",\"Version\":\"0.0\"}";
				var jsonTextReader = new JsonTextReader(new StringReader(input));
				jsonTextReader.Read();
				var result = _sut.ReadJson(jsonTextReader, typeof(StrongName), null, new JsonSerializer());

				Assert.IsInstanceOf<StrongName>(result);
			}
		}
	}
}