﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ManifestLoaderTests.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ManifestLoaderTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Host.Tests
{
	using System.IO;
	using System.Linq;

	using global::ComponentManager.Implementations;

	using NUnit.Framework;

	public sealed class ManifestLoaderTests
	{
		private ManifestLoaderTests()
		{
		}

		public class GivenAManifestLoader
		{
			private const string ManifestPath = "testmanifest.json";
			private string _tempPath;
			private ManifestLoader _loader;

			[SetUp]
			public void Setup()
			{
				_tempPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetTempFileName()));
				_tempPath.EnsureDirectoryExists();
				Path.Combine(_tempPath, "WebServer").EnsureDirectoryExists();
				_loader = new ManifestLoader(_tempPath);
			}

			[TearDown]
			public void Teardown()
			{
				_tempPath.DeleteDirectory();
			}

			[Test]
			public void WhenValidManifestExistsThenLoads()
			{
				var path = Path.GetFullPath(ManifestPath);
				var manifests = _loader.Load(path);

				CollectionAssert.IsNotEmpty(manifests);
			}

			[Test]
			public void WhenValidManifestExistsThenSetsPassword()
			{
				var path = Path.GetFullPath(ManifestPath);
				var manifests = _loader.Load(path);

				Assert.IsTrue(manifests.All(x => x.Password.Length > 0));
			}
		}
	}
}
