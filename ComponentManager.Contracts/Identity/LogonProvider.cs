// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogonProvider.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the LogonProvider type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts.Identity
{
	internal enum LogonProvider
	{
		/// <summary>
		/// Use the standard logon provider for the system.
		/// The default security provider is negotiate, unless you pass NULL for the domain name and the user name
		/// is not in UPN format. In this case, the default provider is NTLM.
		/// NOTE: Windows 2000/NT:   The default security provider is NTLM.
		/// </summary>
		Default = 0,
	}
}