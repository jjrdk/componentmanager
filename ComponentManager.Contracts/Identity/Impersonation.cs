// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Impersonation.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   An impersonation class (modified from http://born2code.net/?page_id=45) that supports LocalService and NetworkService logons.
//   Note: To use these built-in logons the code must be running under the local system account.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts.Identity
{
	using System;
	using System.ComponentModel;
	using System.Diagnostics.CodeAnalysis;
	using System.Runtime.InteropServices;
	using System.Security;
	using System.Security.Principal;

	/// <summary>
	/// An impersonation class (modified from http://born2code.net/?page_id=45) that supports LocalService and NetworkService logons.
	/// Note: To use these built-in logons the code must be running under the local system account.
	/// </summary>
	[ExcludeFromCodeCoverage]
	internal sealed class Impersonation : IDisposable
	{
		/// <summary>
		/// <c>true</c> if disposed; otherwise, <c>false</c>.
		/// </summary>
		private bool _disposed;

		/// <summary>
		/// Holds the created impersonation context and will be used
		/// for reverting to previous user.
		/// </summary>
		private WindowsImpersonationContext _impersonationContext;

		/// <summary>
		/// Initializes a new instance of the <see cref="Impersonation"/> class and
		/// impersonates as a built in service account.
		/// </summary>
		/// <param name="builtinUser">The built in user to impersonate - either
		/// Local Service or Network Service. These users can only be impersonated
		/// by code running as System.</param>
		public Impersonation(BuiltinUser builtinUser)
			: this(string.Empty, "NT AUTHORITY", IntPtr.Zero, LogonType.Service, builtinUser)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Impersonation"/> class and impersonates with the specified credentials.
		/// </summary>
		/// <param name="username">This is the name of the user account to log on to. If you use the user principal name (UPN) format, user@DNS_domain_name, the lpszDomain parameter must be <c>null</c>.</param>
		/// <param name="domain">The name of the domain or server whose account database contains the lpszUsername account. If this parameter is <c>null</c>, the user name must be specified in UPN format. If this parameter is ".", the function validates the account by using only the local account database.</param>
		/// <param name="password">The plaintext password for the user account.</param>
		public Impersonation(string username, string domain, IntPtr password)
			: this(username, domain, password, LogonType.Interactive, BuiltinUser.None)
		{
		}

		[SecuritySafeCritical]
		private Impersonation(string username, string domain, IntPtr password, LogonType logonType, BuiltinUser builtinUser)
		{
			switch (builtinUser)
			{
				case BuiltinUser.None:
					if (string.IsNullOrEmpty(username))
					{
						return;
					}

					break;
				case BuiltinUser.LocalService:
					username = "LOCAL SERVICE";
					break;
				case BuiltinUser.NetworkService:
					username = "NETWORK SERVICE";
					break;
			}

			IntPtr userToken;
			var userTokenDuplication = IntPtr.Zero;

			// Logon with user and get token.
			var loggedOn = SafeNativeMethods.LogonUser(username, domain, ConvertToUnsecureString(password), logonType, LogonProvider.Default, out userToken);

			if (loggedOn)
			{
				try
				{
					// Create a duplication of the usertoken, this is a solution
					// for the known bug that is published under KB article Q319615.
					if (SafeNativeMethods.DuplicateToken(userToken, 2, ref userTokenDuplication))
					{
						// Create windows identity from the token and impersonate the user.
						var identity = new WindowsIdentity(userTokenDuplication);
						_impersonationContext = identity.Impersonate();
					}
					else
					{
						// Token duplication failed!
						// Use the default ctor overload that will use Mashal.GetLastWin32Error(); to create the exceptions details.
						throw new Win32Exception();
					}
				}
				finally
				{
					// Close usertoken handle duplication when created.
					if (!userTokenDuplication.Equals(IntPtr.Zero))
					{
						// Closes the handle of the user.
						SafeNativeMethods.CloseHandle(userTokenDuplication);
						userTokenDuplication = IntPtr.Zero;
					}

					// Close usertoken handle when created.
					if (!userToken.Equals(IntPtr.Zero))
					{
						// Closes the handle of the user.
						SafeNativeMethods.CloseHandle(userToken);
						userToken = IntPtr.Zero;
					}
				}
			}
			else
			{
				// Logon failed!
				// Use the default ctor overload that will use Mashal.GetLastWin32Error(); to create the exceptions details.
				throw new Win32Exception();
			}
		}

		/// <summary>
		/// Releases unmanaged resources and performs other cleanup operations before the
		/// <see cref="Impersonation"/> is reclaimed by garbage collection.
		/// </summary>
		~Impersonation()
		{
			Dispose(false);
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or
		/// resetting unmanaged resources and will revent to the previous user when
		/// the impersonation still exists.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		[SecuritySafeCritical]
		private static string ConvertToUnsecureString(IntPtr passwordPointer)
		{
			return passwordPointer == IntPtr.Zero ? string.Empty : Marshal.PtrToStringUni(passwordPointer);
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or
		/// resetting unmanaged resources and will revent to the previous user when
		/// the impersonation still exists.
		/// </summary>
		/// <param name="disposing">Specify <c>true</c> when calling the method directly or indirectly by a user�s code; Otherwise <c>false</c>.</param>
		private void Dispose(bool disposing)
		{
			if (disposing)
			{
			}

			if (!_disposed)
			{
				Revert();

				_disposed = true;
			}
		}

		/// <summary>
		/// Reverts to the previous user.
		/// </summary>
		private void Revert()
		{
			if (_impersonationContext != null)
			{
				// Revert to previour user.
				_impersonationContext.Undo();
				_impersonationContext = null;
			}
		}
	}
}