// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImpersonationFactory.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ImpersonationFactory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts.Identity
{
	using System;

	internal class ImpersonationFactory
	{
		public Impersonation Create(string username, string domain, IntPtr password)
		{
			BuiltinUser builtinUser;
			if (Enum.TryParse(username, true, out builtinUser))
			{
				return new Impersonation(builtinUser);
			}
			
			return new Impersonation(username, domain, password);
		}
	}
}