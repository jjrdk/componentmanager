// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageComponentBase.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the MessageComponentBase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System;
	using System.Diagnostics.CodeAnalysis;

	[SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Justification = "No disposable override necessary.")]
	public abstract class MessageComponentBase : ComponentBase, IMessageComponent
	{
		protected MessageComponentBase(TimeSpan initializationTimeout, params Uri[] baseServiceAddresses)
			: base(initializationTimeout, baseServiceAddresses)
		{
		}

		public virtual string Filter
		{
			get
			{
				return string.Empty;
			}
		}

		protected abstract IObservable<IEnvelope> MessageSource { get; }

		protected abstract void Publish(string target, string correlationId, string subject, object item);

		protected abstract void Publish(string target, string subject, object item);
	}
}