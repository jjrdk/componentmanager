// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceProvider.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ServiceProvider type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System;
	using System.Collections.Concurrent;
	using System.Linq;
	using System.Security;
	using System.ServiceModel;
	using System.ServiceModel.Channels;
	using System.ServiceModel.Discovery;

	internal class ServiceProvider : IDisposable
	{
		private readonly ConcurrentDictionary<Type, ChannelFactory> _knownChannelFactories = new ConcurrentDictionary<Type, ChannelFactory>();

		~ServiceProvider()
		{
			Dispose(false);
		}

		[SecuritySafeCritical]
		public T Get<T>()
		{
			var factory = _knownChannelFactories.GetOrAdd(
				typeof(T),
				CreateChannelFactory<T>) as ChannelFactory<T>;

			if (factory == null)
			{
				return default(T);
			}

			if (factory.State == CommunicationState.Closed
				|| factory.State == CommunicationState.Closing
				|| factory.State == CommunicationState.Faulted)
			{
				ChannelFactory brokenFactory;
				_knownChannelFactories.TryRemove(typeof(T), out brokenFactory);

				return Get<T>();
			}

			var proxy = factory.CreateChannel();

			return proxy;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				foreach (var factory in _knownChannelFactories.Values)
				{
					factory.Close();
				}

				_knownChannelFactories.Clear();
			}

			// get rid of unmanaged resources
		}

		[SecuritySafeCritical]
		private ChannelFactory CreateChannelFactory<T>(Type t)
		{
			var discoveryClient = new DiscoveryClient(new UdpDiscoveryEndpoint());
			var criteria = new FindCriteria(t);
			var discovered = discoveryClient.Find(criteria);
			discoveryClient.Close();
			if (!discovered.Endpoints.Any())
			{
				return null;
			}

			EndpointAddress address = discovered.Endpoints[0].Address;
			Binding binding = GetBinding(address.Uri);
			if (binding == null)
			{
				return null;
			}

			var timeout = TimeSpan.FromMinutes(10);
			binding.ReceiveTimeout = timeout;
			binding.OpenTimeout = timeout;
			binding.CloseTimeout = timeout;
			binding.SendTimeout = timeout;

			var typedFactory = new ChannelFactory<T>(binding, address);

			return typedFactory;
		}

		private Binding GetBinding(Uri uri)
		{
			if (uri.Scheme == Uri.UriSchemeHttp)
			{
				return new BasicHttpBinding();
			}

			if (uri.Scheme == Uri.UriSchemeHttps)
			{
				return new BasicHttpsBinding();
			}

			if (uri.Scheme == Uri.UriSchemeNetTcp)
			{
				return new NetTcpBinding();
			}

			if (uri.Scheme == Uri.UriSchemeNetPipe)
			{
				return new NetNamedPipeBinding();
			}

			return null;
		}
	}
}