﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentBase.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ComponentBase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System;
	using System.ServiceModel;

	public abstract class ComponentBase : MarshalByRefObject, IComponent
	{
		private readonly ServiceProvider _provider;
		private readonly ServiceHost _host;
		private ComponentMole _mole;

		protected ComponentBase(TimeSpan initializationTimeout, params Uri[] baseServiceAddresses)
		{
			InitializationTimeout = initializationTimeout;

			var hostFactory = new ServiceHostFactory();
			_host = hostFactory.CreateServiceHost(new HostConfiguration(this, TimeSpan.FromMinutes(10), baseServiceAddresses));

			_provider = new ServiceProvider();
		}

		~ComponentBase()
		{
			Dispose(false);
		}

		public TimeSpan InitializationTimeout { get; private set; }

		public virtual void Run(ComponentParameters parameters)
		{
			_mole = new ComponentMole(parameters.Username, parameters.Domain, parameters.PasswordPointer, parameters.InitializationWaitHandleName);
		}

		public abstract void Stop();

		public void Dispose()
		{
			Dispose(true);
		}

		protected void SetInitializationComplete()
		{
			_mole.SetWaitHandle();
		}

		protected T GetService<T>()
		{
			return _provider.Get<T>();
		}

		protected virtual void Dispose(bool isDisposing)
		{
			if (isDisposing)
			{
				Stop();
				_provider.Dispose();

				if (_host != null)
				{
					_host.Close();
				}

				if (_mole != null)
				{
					_mole.Dispose();
				}
			}
		}
	}
}