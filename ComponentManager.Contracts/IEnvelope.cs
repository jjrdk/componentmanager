﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEnvelope.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the IMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System;
	using System.Security;

	[SecuritySafeCritical]
	public interface IEnvelope
	{
		string ID { get; }

		string Target { get; }

		string Subject { get; }

		string CorrelationID { get; }

		DateTime Sent { get; }

		TimeSpan MaxAge { get; }

		T GetPayload<T>();

		object GetPayload();
	}
}