// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentManifest.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ComponentManifest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System.Collections.Generic;
	using System.Linq;
	using System.Security;
	using System.Security.Policy;

	public class ComponentManifest
	{
		public ComponentManifest(string name, string path, string assembly, string type, string username, string domain, SecureString password, SecurityElement permissions, IEnumerable<StrongName> strongNames)
		{
			Name = name;
			Path = path;
			Assembly = assembly;
			Type = type;
			Username = username;
			Domain = domain;
			Password = password;
			Permissions = permissions;
			StrongNames = strongNames.ToArray();
		}

		public string Name { get; private set; }

		public string Path { get; private set; }

		public string Assembly { get; private set; }

		public string Type { get; private set; }

		public string Username { get; private set; }

		public SecureString Password { get; private set; }

		public string Domain { get; private set; }

		public SecurityElement Permissions { get; private set; }

		public StrongName[] StrongNames { get; private set; }

		public void UpdatePath(string appDir)
		{
			Path = System.IO.Path.GetFullPath(System.IO.Path.Combine(appDir, Path));
		}
	}
}