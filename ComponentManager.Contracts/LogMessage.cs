﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogMessage.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the LogMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System;

	[Serializable]
	public class LogMessage
	{
		public LogMessage(int priority, string message)
		{
			Logged = DateTime.UtcNow;
			Priority = priority;
			Message = message;
		}

		public DateTime Logged { get; private set; }

		public int Priority { get; private set; }

		public string Message { get; private set; }		
	}
}