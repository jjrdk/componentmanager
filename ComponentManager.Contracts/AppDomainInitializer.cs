// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppDomainInitializer.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the AppDomainInitializer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System;
	using System.Linq;

	public static class AppDomainInitializer
	{
		public static void Initialize(string[] args)
		{
			AppDomainSettings.EndpointPrefix = args[0];
			AppDomainSettings.BaseServiceAddresses = args.Skip(1).Select(x => new Uri(x)).ToArray();
		}
	}
}