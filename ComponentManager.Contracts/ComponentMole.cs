﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentMole.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ComponentMole type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System;
	using System.Diagnostics.CodeAnalysis;
	using System.Security;
	using System.Security.Principal;
	using System.Threading;

	using ComponentManager.Contracts.Identity;

	internal sealed class ComponentMole : IDisposable
	{
		private readonly Impersonation _impersonation;
		private readonly EventWaitHandle _waitHandle;

		[SecuritySafeCritical]
		public ComponentMole(string username, string domain, IntPtr password, string waitHandleName)
		{
			_waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset, waitHandleName);
			var impersonationFactory = new ImpersonationFactory();
			_impersonation = impersonationFactory.Create(username, domain, password);
			var currentIdentity = WindowsIdentity.GetCurrent(true);
			var currentDomain = AppDomain.CurrentDomain;

			var principal = currentIdentity == null ? Thread.CurrentPrincipal : new WindowsPrincipal(currentIdentity);

			currentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
			currentDomain.SetThreadPrincipal(principal);
			Thread.CurrentPrincipal = principal;
		}

		[ExcludeFromCodeCoverage]
		~ComponentMole()
		{
			Dispose(false);
		}

		public void SetWaitHandle()
		{
			_waitHandle.Set();
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool isDisposing)
		{
			if (isDisposing)
			{
				_waitHandle.Dispose();
				_impersonation.Dispose();
			}
		}
	}
}