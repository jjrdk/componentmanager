﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SubscriptionRequest.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the SubscriptionRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System;

	[Serializable]
	public class SubscriptionRequest
	{
		public string EndPoint { get; set; }

		public string Filter { get; set; }
	}
}