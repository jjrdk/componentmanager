// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceHostFactory.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ServiceHostFactory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System;
	using System.Linq;
	using System.Security;
	using System.ServiceModel;
	using System.ServiceModel.Discovery;

	internal class ServiceHostFactory
	{
		[SecuritySafeCritical]
		public ServiceHost CreateServiceHost(HostConfiguration configuration)
		{
			var instanceType = configuration.InstanceType;
			if (!HasServiceContracts(instanceType) || !configuration.BaseAddresses.Any())
			{
				return null;
			}

			var host = new ServiceHost(configuration.Instance, configuration.BaseAddresses);
			host.AddDefaultEndpoints();
			host.AddServiceEndpoint(new UdpDiscoveryEndpoint());
			host.Description.Behaviors.Add(new ServiceDiscoveryBehavior());
			var behaviour = host.Description.Behaviors.Find<ServiceBehaviorAttribute>();
			behaviour.InstanceContextMode = InstanceContextMode.Single;

			foreach (var binding in host.Description.Endpoints.Select(x => x.Binding))
			{
				binding.OpenTimeout = configuration.Timeout;
				binding.CloseTimeout = configuration.Timeout;
				binding.ReceiveTimeout = configuration.Timeout;
				binding.SendTimeout = configuration.Timeout;
			}

			host.Open();

			return host;
		}

		private static bool HasServiceContracts(Type type)
		{
			var serviceContracts = from @interface in type.GetInterfaces().Concat(new[] { type })
								   where @interface.IsInterface
								   from serviceContract in @interface.GetCustomAttributes(typeof(ServiceContractAttribute), true)
								   select serviceContract;

			return serviceContracts.Any();
		}
	}
}