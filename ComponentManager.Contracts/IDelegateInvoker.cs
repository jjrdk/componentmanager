﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDelegateInvoker.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the IDelegateInvoker type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System;
	using System.Threading.Tasks;

	public interface IDelegateInvoker : IDisposable
	{
		/// <summary>
		/// Executes the operation.
		/// </summary>
		/// <param name="action">Operation to execute.</param>
		Task Execute(Action action);

		/// <summary>
		/// Executes the operation.
		/// </summary>
		/// <param name="action">Operation to execute.</param>
		/// <returns>Result of operation as an object.</returns>
		Task<T> Execute<T>(Func<T> action);
	}
}