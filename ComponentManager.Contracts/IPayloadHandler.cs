﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPayloadHandler.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the IPayloadHandler type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System.Threading.Tasks;

	public interface IPayloadHandler
	{
		Task Handle<T>(T payload);
	}
}