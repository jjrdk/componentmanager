// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IResilienceChallenger.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the IResilienceChallenger type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System;
	using System.Collections.Generic;

	public interface IResilienceChallenger : IDisposable
	{
		void Start(IEnumerable<ComponentManifest> manifests);

		void Stop();
	}
}