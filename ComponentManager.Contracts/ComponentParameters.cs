﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentParameters.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ComponentParameters type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System;
	
	[Serializable]
	public sealed class ComponentParameters
	{
		public ComponentParameters(string publishQueueName, string receiveQueueName, string initializationWaitHandleName, string username, string domain, IntPtr passwordPointer)
		{
			PublishQueueName = publishQueueName;
			ReceiveQueueName = receiveQueueName;
			InitializationWaitHandleName = initializationWaitHandleName;
			Username = username;
			Domain = domain;
			PasswordPointer = passwordPointer;
		}

		public string PublishQueueName { get; private set; }

		public string ReceiveQueueName { get; private set; }

		public string InitializationWaitHandleName { get; private set; }

		public string Username { get; private set; }

		public string Domain { get; private set; }

		public IntPtr PasswordPointer { get; private set; }
	}
}