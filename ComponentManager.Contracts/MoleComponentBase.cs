// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MoleComponentBase.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the MoleComponentBase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System;

	public abstract class MoleComponentBase : MessageComponentBase
	{
		[NonSerialized]
		private readonly Mole _messageSource;

		public MoleComponentBase(TimeSpan initializationTimeout, Mole mole, params Uri[] baseServiceAddresses)
			: base(initializationTimeout, baseServiceAddresses)
		{
			_messageSource = mole;
		}

		protected override IObservable<IEnvelope> MessageSource
		{
			get
			{
				return _messageSource;
			}
		}

		public override void Run(ComponentParameters parameters)
		{
			base.Run(parameters);

			_messageSource.Start();
		}

		protected override void Publish(string target, string correlationId, string subject, object item)
		{
			_messageSource.Publish(target, correlationId, subject, TimeSpan.MaxValue, item);
		}

		protected override void Publish(string target, string subject, object item)
		{
			Publish(target, string.Empty, subject, item);
		}

		protected override void Dispose(bool isDisposing)
		{
			base.Dispose(isDisposing);
			if (isDisposing)
			{
				_messageSource.Dispose();
			}
		}
	}
}