// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HostConfiguration.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the HostConfiguration type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System;

	internal class HostConfiguration
	{
		public HostConfiguration(object instance, TimeSpan timeout, params Uri[] baseAddresses)
		{
			if (instance == null)
			{
				throw new ArgumentNullException("instance");
			}

			Instance = instance;
			InstanceType = instance.GetType();
			Timeout = timeout;
			BaseAddresses = baseAddresses;
		}

		public object Instance { get; private set; }

		public TimeSpan Timeout { get; private set; }

		public Uri[] BaseAddresses { get; private set; }

		public Type InstanceType { get; private set; }
	}
}