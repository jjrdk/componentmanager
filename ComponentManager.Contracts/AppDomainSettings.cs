// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppDomainSettings.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the AppDomainSettings type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Contracts
{
	using System;

	public static class AppDomainSettings
	{
		public static Uri[] BaseServiceAddresses { get; internal set; }

		public static string EndpointPrefix { get; internal set; }
	}
}