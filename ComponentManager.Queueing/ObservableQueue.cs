// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObservableQueue.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ObservableQueue type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Queueing
{
	using System;
	using System.Messaging;
	using System.Reactive.Subjects;
	using System.Security;
	using System.Security.Permissions;

	using ComponentManager.Contracts;

	public sealed class ObservableQueue : IObservable<IEnvelope>, IDisposable
	{
		private readonly Subject<IEnvelope> _messageSubject = new Subject<IEnvelope>();
		private readonly MessageQueue _queue;
		private bool _isDisposed;

		[SecuritySafeCritical]
		public ObservableQueue(QueueFactory queueFactory, string queueName)
		{
			_queue = queueFactory.Create(queueName);
		}

		~ObservableQueue()
		{
			Dispose(false);
		}

		[SecuritySafeCritical]
		public void Send(IEnvelope envelope)
		{
			var queueEnvelope = new QueueEnvelope(
				envelope.ID,
				envelope.CorrelationID,
				envelope.Target,
				envelope.Subject,
				envelope.Sent,
				envelope.MaxAge,
				envelope.GetPayload());

			_messageSubject.OnNext(queueEnvelope);
		}

		/// <summary>
		/// Notifies the provider that an observer is to receive notifications.
		/// </summary>
		/// <returns>
		/// A reference to an interface that allows observers to stop receiving notifications before the provider has finished sending them.
		/// </returns>
		/// <param name="observer">The object that is to receive notifications.</param>
		[SecuritySafeCritical]
		public IDisposable Subscribe(IObserver<IEnvelope> observer)
		{
			return _messageSubject.Subscribe(observer);
		}

		[SecuritySafeCritical]
		public void Start()
		{
			var permission = new MessageQueuePermission(MessageQueuePermissionAccess.Receive, _queue.Path);
			permission.Assert();

			var existingMessages = _queue.GetAllMessages();
			foreach (var message in existingMessages)
			{
				PublishMessage(message);
			}

			ListenReceive();
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		[SecuritySafeCritical]
		private void Dispose(bool disposing)
		{
			if (_isDisposed)
			{
				return;
			}

			if (disposing)
			{
				_messageSubject.Dispose();
				_isDisposed = true;
				_queue.Dispose();
			}

			// get rid of unmanaged resources
		}

		[SecuritySafeCritical]
		private void ListenReceive()
		{
			var permission = new MessageQueuePermission(MessageQueuePermissionAccess.Receive, _queue.Path);
			permission.Assert();

			_queue.BeginReceive(MessageQueue.InfiniteTimeout, null, OnReceive);
		}

		[SecuritySafeCritical]
		private void OnReceive(IAsyncResult ar)
		{
			var message = GetMessage(ar);

			if (!_isDisposed && message != null)
			{
				PublishMessage(message);
			}

			if (!_isDisposed)
			{
				ListenReceive();
			}
		}

		[SecuritySafeCritical]
		[SecurityPermission(SecurityAction.Assert, SerializationFormatter = true)]
		private Message GetMessage(IAsyncResult ar)
		{
			Message message = null;

			try
			{
				message = _queue.EndReceive(ar);
			}
			catch (MessageQueueException)
			{
			}
			catch (TimeoutException)
			{
				// retry?
			}

			return message;
		}

		[SecuritySafeCritical]
		[SecurityPermission(SecurityAction.Assert, SerializationFormatter = true)]
		private void PublishMessage(Message msg)
		{
			var permission = new MessageQueuePermission(MessageQueuePermissionAccess.Send, _queue.Path);
			permission.Assert();

			var envelope = msg.Body as IEnvelope;
			if (envelope != null)
			{
				_messageSubject.OnNext(envelope);
			}
		}
	}
}