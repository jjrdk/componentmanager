// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageQueueMole.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the MessageQueueMole type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Queueing
{
	using System;
	using System.Messaging;
	using System.Security;
	using System.Security.Permissions;

	using ComponentManager.Contracts;

	[Serializable]
	[SecuritySafeCritical]
	public sealed class MessageQueueMole : Mole
	{
		private readonly IEndPointProvider _endPointProvider;
		[SecuritySafeCritical]
		private readonly MessageQueue _publishQueue;
		[SecuritySafeCritical]
		private readonly ObservableQueue _source;

		[SecuritySafeCritical]
		public MessageQueueMole(string endPointName, IEndPointProvider endPointProvider)
		{
			_endPointProvider = endPointProvider;
			var queueFactory = new QueueFactory();
			var receiveQueueName = _endPointProvider.GetReceiveEndPoint(endPointName);
			_publishQueue = queueFactory.Create(_endPointProvider.GetPublishEndPoint());

			_source = new ObservableQueue(queueFactory, receiveQueueName);
		}

		~MessageQueueMole()
		{
			Dispose(false);
		}

		[SecuritySafeCritical]
		public override void Start()
		{
			_source.Start();
		}

		[SecuritySafeCritical]
		[SecurityPermission(SecurityAction.Assert, SerializationFormatter = true)]
		public override void Publish(string target, string correlationId, string subject, TimeSpan maxAge, object item)
		{
			var permission = new MessageQueuePermission(MessageQueuePermissionAccess.Send, _endPointProvider.GetPublishEndPoint());
			permission.Assert();

			var envelope = new QueueEnvelope(
				Guid.NewGuid().ToString("N"),
				correlationId,
				target,
				subject,
				DateTime.UtcNow,
				maxAge,
				item);
			var msg = new Message(envelope, _publishQueue.Formatter)
						  {
							  CorrelationId = correlationId,
							  Recoverable = true,
							  TimeToBeReceived = maxAge,
							  UseDeadLetterQueue = true,
							  UseJournalQueue = false
						  };
			_publishQueue.Send(msg);
		}

		/// <summary>
		/// Notifies the provider that an observer is to receive notifications.
		/// </summary>
		/// <returns>
		/// A reference to an interface that allows observers to stop receiving notifications before the provider has finished sending them.
		/// </returns>
		/// <param name="observer">The object that is to receive notifications.</param>
		[SecuritySafeCritical]
		public override IDisposable Subscribe(IObserver<IEnvelope> observer)
		{
			return _source.Subscribe(observer);
		}

		[SecuritySafeCritical]
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				_source.Dispose();
				_publishQueue.Dispose();
			}
		}
	}
}