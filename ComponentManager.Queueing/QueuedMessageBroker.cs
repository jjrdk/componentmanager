﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueuedMessageBroker.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the QueuedMessageBroker type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Queueing
{
	using System;
	using System.Messaging;
	using System.Reactive.Linq;
	using System.Security;

	using ComponentManager.Contracts;

	using Linq2Rest;

	[SecuritySafeCritical]
	public class QueuedMessageBroker : IMessageBroker
	{
		private readonly ObservableQueue _queue;
		private readonly QueueFactory _factory;
		private readonly ODataExpressionConverter _converter;

		[SecuritySafeCritical]
		public QueuedMessageBroker(IEndPointProvider endPointProvider, QueueFactory factory)
		{
			_converter = new ODataExpressionConverter();
			_queue = new ObservableQueue(factory, endPointProvider.GetPublishEndPoint());
			_factory = factory;
		}

		~QueuedMessageBroker()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		public void Start()
		{
			_queue.Start();
		}

		[SecuritySafeCritical]
		public void Send(IEnvelope envelope)
		{
			_queue.Send(envelope);
		}

		[SecuritySafeCritical]
		public IDisposable Subscribe(SubscriptionRequest subscriptionRequest)
		{
			var filterExpression = _converter.Convert<IEnvelope>(subscriptionRequest.Filter);
			var predicate = filterExpression.Compile();
			return _queue
				.Where(
					x =>
					{
						try
						{
							return x.MaxAge == TimeSpan.MaxValue || (x.Sent.ToUniversalTime() + x.MaxAge) >= DateTime.UtcNow;
						}
						catch
						{
							return true;
						}
					})
				.Where(predicate)
				.Subscribe(new MessageHandler(_factory, subscriptionRequest.EndPoint));
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				_queue.Dispose();
			}
		}

		private class MessageHandler : IObserver<IEnvelope>, IDisposable
		{
			private readonly MessageQueue _queue;

			public MessageHandler(QueueFactory queueFactory, string endPoint)
			{
				_queue = queueFactory.Create(endPoint);
			}

			~MessageHandler()
			{
				Dispose(false);
			}

			public void Dispose()
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}

			/// <summary>
			/// Provides the observer with new data.
			/// </summary>
			/// <param name="value">The current notification information.</param>
			[SecuritySafeCritical]
			public void OnNext(IEnvelope value)
			{
				var msg = new Message(value, _queue.Formatter)
				{
					CorrelationId = value.CorrelationID ?? string.Empty,
					Recoverable = true,
					TimeToBeReceived = TimeSpan.MaxValue,
					UseDeadLetterQueue = true,
					UseJournalQueue = false
				};
				_queue.Send(msg);
			}

			/// <summary>
			/// Notifies the observer that the provider has experienced an error condition.
			/// </summary>
			/// <param name="error">An object that provides additional information about the error.</param>
			public void OnError(Exception error)
			{
			}

			/// <summary>
			/// Notifies the observer that the provider has finished sending push-based notifications.
			/// </summary>
			public void OnCompleted()
			{
				_queue.Dispose();
			}

			private void Dispose(bool disposing)
			{
				if (disposing)
				{
					_queue.Dispose();
				}
			}
		}
	}
}