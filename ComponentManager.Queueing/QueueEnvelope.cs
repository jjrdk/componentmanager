﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueueEnvelope.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the QueueEnvelope type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Queueing
{
	using System;
	using System.Security;

	using ComponentManager.Contracts;

	using Newtonsoft.Json;

	[Serializable]
	[SecuritySafeCritical]
	internal class QueueEnvelope : IEnvelope
	{
		public QueueEnvelope(string id, string correlationID, string target, string subject, DateTime sent, TimeSpan maxAge, object body)
		{
			ID = id;
			CorrelationID = correlationID;
			Target = target;
			Subject = subject;
			Sent = sent;
			MaxAge = maxAge;
			if (body != null)
			{
				PayloadType = body.GetType().AssemblyQualifiedName;
				Body = JsonConvert.SerializeObject(body);
			}
		}

		public string ID { get; private set; }

		public string CorrelationID { get; private set; }

		public string Target { get; private set; }

		public string PayloadType { get; private set; }

		public string Subject { get; private set; }

		public DateTime Sent { get; private set; }

		public TimeSpan MaxAge { get; private set; }

		public string Body { get; private set; }

		public T GetPayload<T>()
		{
			return JsonConvert.DeserializeObject<T>(Body);
		}

		public object GetPayload()
		{
			var type = Type.GetType(PayloadType, false, true);
			return JsonConvert.DeserializeObject(Body, type);
		}

		/// <summary>
		/// Returns a string that represents the current object.
		/// </summary>
		/// <returns>
		/// A string that represents the current object.
		/// </returns>
		public override string ToString()
		{
			return string.Format(
@"ID: {0},
CorrelationID: {1},
Target: {6},
Subject: {2},
Sent: {3},
Max Age: {7},
Payload Type: {4},
Body: {5}",
		  ID,
		  CorrelationID,
		  Subject,
		  Sent,
		  PayloadType,
		  Body,
		  Target,
		  MaxAge);
		}
	}
}