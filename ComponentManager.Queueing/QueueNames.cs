﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueueNames.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the QueueNames type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Queueing
{
	using System;
	using System.Security;

	using ComponentManager.Contracts;

	[SecuritySafeCritical]
	public class QueueNames : IEndPointProvider
	{
		private readonly string _publishQueuePath;
		private readonly string _prefix;

		public QueueNames(string prefix)
		{
			_prefix = prefix;
			_publishQueuePath = @".\Private$\publisher";
		}

		public string Prefix
		{
			get
			{
				return _prefix;
			}
		}

		public string GetPublishEndPoint()
		{
			return _publishQueuePath;
		}

		public string GetReceiveEndPoint(string name)
		{
			return @".\Private$\" + _prefix + "." + name;
		}

		public string[] GetBaseServiceAddresses(string name)
		{
			return new[]
					   {
						   string.Format(
							   "net.pipe://{0}/{1}",
							   Environment.MachineName,
							   name)
					   };
		}
	}
}