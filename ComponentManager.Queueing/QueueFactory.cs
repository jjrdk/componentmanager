// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueueFactory.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the QueueFactory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Queueing
{
	using System.Messaging;
	using System.Runtime.Serialization.Formatters;
	using System.Security;
	using System.Security.Permissions;

	public sealed class QueueFactory
	{
		[SecuritySafeCritical]
		[MessageQueuePermission(SecurityAction.Assert, Unrestricted = true)]
		public MessageQueue Create(string name)
		{
			var queue = MessageQueue.Exists(name)
				? new MessageQueue(name)
				: MessageQueue.Create(name);

			queue.Formatter = new BinaryMessageFormatter(FormatterAssemblyStyle.Full, FormatterTypeStyle.TypesAlways);
			queue.DefaultPropertiesToSend.Recoverable = true;
			queue.MessageReadPropertyFilter.SetAll();

			return queue;
		}
	}
}