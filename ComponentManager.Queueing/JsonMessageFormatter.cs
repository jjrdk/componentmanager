﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JsonMessageFormatter.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the JsonMessageFormatter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Queueing
{
	using System;
	using System.IO;
	using System.Messaging;
	using System.Security;
	using System.Text;

	using Newtonsoft.Json;

	[SecurityCritical]
	public class JsonMessageFormatter : IMessageFormatter
	{
		private static readonly JsonSerializerSettings DefaultSerializerSettings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.None };
		private readonly JsonSerializerSettings _serializerSettings;
		private readonly Encoding _encoding;

		public JsonMessageFormatter(Encoding encoding)
			: this(encoding, null)
		{
		}

		private JsonMessageFormatter(Encoding encoding, JsonSerializerSettings serializerSettings)
		{
			_encoding = encoding ?? Encoding.UTF8;
			_serializerSettings = serializerSettings ?? DefaultSerializerSettings;
		}
		
		[SecurityCritical]
		public bool CanRead(Message message)
		{
			if (message == null)
			{
				throw new ArgumentNullException("message");
			}

			var stream = message.BodyStream;

			return stream != null
				   && stream.CanRead
				   && stream.Length > 0;
		}

		[SecuritySafeCritical]
		public object Clone()
		{
			return new JsonMessageFormatter(_encoding, _serializerSettings);
		}

		[SecurityCritical]
		public object Read(Message message)
		{
			if (message == null)
			{
				throw new ArgumentNullException("message");
			}

			if (!CanRead(message))
			{
				return null;
			}

			using (var reader = new StreamReader(message.BodyStream, _encoding))
			{
				var json = reader.ReadToEnd();
				return JsonConvert.DeserializeObject(json, _serializerSettings);
			}
		}

		[SecurityCritical]
		public void Write(Message message, object obj)
		{
			if (message == null)
			{
				throw new ArgumentNullException("message");
			}

			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}

			var json = JsonConvert.SerializeObject(obj, Formatting.None, _serializerSettings);

			message.BodyStream = new MemoryStream(_encoding.GetBytes(json));
			message.BodyType = 0;
		}
	}
}