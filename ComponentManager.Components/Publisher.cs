// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Publisher.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the Publisher type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Components
{
	using System;
	using System.Threading;
	using System.Threading.Tasks;
	using System.Timers;

	using ComponentManager.Contracts;
	using ComponentManager.Implementations;

	using Timer = System.Timers.Timer;

	[Serializable]
	internal abstract class Publisher : MoleComponentBase
	{
		[NonSerialized]
		private readonly Timer _timer;
		[NonSerialized]
		private readonly ManualResetEventSlim _waitHandle = new ManualResetEventSlim(false);

		public Publisher(Mole mole, params Uri[] baseServiceAddresses)
			: base(TimeSpan.FromSeconds(15), mole, baseServiceAddresses)
		{
			_timer = new Timer(3000);
			_timer.Elapsed += OnTimer;
		}

		public override void Run(ComponentParameters parameters)
		{
			base.Run(parameters);
			_timer.Start();
			SetInitializationComplete();
			OnTimer(this, null);
			_waitHandle.Wait();
			_waitHandle.Dispose();
		}

		public override void Stop()
		{
			_waitHandle.Set();
		}

		protected override void Dispose(bool isDisposing)
		{
			base.Dispose(isDisposing);
			if (isDisposing)
			{
				_waitHandle.Dispose();
				_timer.Stop();
				_timer.Elapsed -= OnTimer;
				_timer.Dispose();
				_waitHandle.Dispose();
			}
		}

		private void OnTimer(object sender, ElapsedEventArgs e)
		{
			var task = PublishValue();
		}

		private async Task PublishValue()
		{
			var policy = new BackoffInvoker(10);
			var sayHello = await policy.Execute<string>(GetValue).ConfigureAwait(false);

			Publish("QueueClient", sayHello, new TimeContainer { Time = DateTime.UtcNow });
			Publish("QueueClient", "Time", new TimeContainer { Time = DateTime.UtcNow });
		}

		private string GetValue()
		{
			var service = GetService<IHelloWorldService>();

			return service.SayHello("Test");
		}
	}
}