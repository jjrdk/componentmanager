﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Client.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the Client type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Components
{
	using System;
	using System.Diagnostics.CodeAnalysis;
	using System.Linq.Expressions;
	using System.Threading;

	using ComponentManager.Contracts;

	using Linq2Rest;

	[Serializable]
	internal abstract class Client : MoleComponentBase
	{
		private readonly IDisposable _subscription;
		private readonly string _filterString;
		[NonSerialized]
		private readonly ManualResetEventSlim _waitHandle = new ManualResetEventSlim(false);

		[SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors", Justification = "Call is safe.")]
		protected Client(Mole mole, params Uri[] baseServiceAddresses)
			: base(TimeSpan.FromSeconds(15), mole, baseServiceAddresses)
		{
			var converter = new ODataExpressionConverter();
			Expression<Func<IEnvelope, bool>> filter = x => x.Subject != "Time";
			_filterString = converter.Convert(filter);
			_subscription = CreateSubscription();
		}

		public override string Filter
		{
			get
			{
				return _filterString;
			}
		}

		public override void Run(ComponentParameters parameters)
		{
			base.Run(parameters);
			SetInitializationComplete();
			_waitHandle.Wait();
			_waitHandle.Dispose();
		}

		public override void Stop()
		{
			_waitHandle.Set();
		}

		protected override void Dispose(bool isDisposing)
		{
			base.Dispose(isDisposing);
			if (isDisposing)
			{
				_waitHandle.Dispose();
				_subscription.Dispose();
				_waitHandle.Dispose();
			}
		}

		private IDisposable CreateSubscription()
		{
			return MessageSource.Subscribe(new MessageObserver());
		}

		private class MessageObserver : IObserver<IEnvelope>
		{
			/// <summary>
			/// Provides the observer with new data.
			/// </summary>
			/// <param name="value">The current notification information.</param>
			public void OnNext(IEnvelope value)
			{
				Console.ForegroundColor = GetColor(value.Subject);
				Console.WriteLine(value.ToString());
				Console.ResetColor();
			}

			/// <summary>
			/// Notifies the observer that the provider has experienced an error condition.
			/// </summary>
			/// <param name="error">An object that provides additional information about the error.</param>
			public void OnError(Exception error)
			{
				Console.WriteLine(error.Message);
			}

			/// <summary>
			/// Notifies the observer that the provider has finished sending push-based notifications.
			/// </summary>
			public void OnCompleted()
			{
			}

			private ConsoleColor GetColor(string subject)
			{
				switch (subject.ToLowerInvariant())
				{
					case "infrastructure":
						return ConsoleColor.Yellow;
					case "resource tracking":
						return ConsoleColor.Green;
					default:
						return ConsoleColor.White;
				}
			}
		}
	}
}