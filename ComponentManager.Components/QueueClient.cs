﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueueClient.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the QueueClient type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Components
{
	using System;

	using ComponentManager.Contracts;
	using ComponentManager.Queueing;

	[Serializable]
	internal sealed class QueueClient : Client, IHelloWorldService
	{
		public QueueClient()
			: base(new MessageQueueMole(typeof(QueueClient).FullName, new QueueNames(AppDomainSettings.EndpointPrefix)), AppDomainSettings.BaseServiceAddresses)
		{
		}

		public string SayHello(string name)
		{
			return "Hello " + name;
		}
	}
}