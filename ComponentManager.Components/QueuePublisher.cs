// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueuePublisher.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the QueuePublisher type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Components
{
	using System;

	using ComponentManager.Contracts;
	using ComponentManager.Queueing;

	[Serializable]
	internal class QueuePublisher : Publisher
	{
		public QueuePublisher()
			: base(new MessageQueueMole(typeof(QueuePublisher).FullName, new QueueNames(AppDomainSettings.EndpointPrefix)))
		{
		}
	}
}