﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestEnvelope.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the TestEnvelope type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Implementations.Tests
{
	using System;

	using ComponentManager.Contracts;

	internal class TestEnvelope : IEnvelope
	{
		private readonly DateTime _sent;
		private readonly TimeSpan _maxAge;
		private readonly object _payload;

		public TestEnvelope(string id, string target, string subject, string correlationID, DateTime sent, TimeSpan maxAge, object payload)
		{
			_sent = sent;
			_maxAge = maxAge;
			_payload = payload;
			ID = id;
			Target = target;
			Subject = subject;
			CorrelationID = correlationID;
		}

		public string ID { get; private set; }

		public string Target { get; private set; }

		public string Subject { get; private set; }

		public string CorrelationID { get; private set; }

		public DateTime Sent { get; private set; }

		public TimeSpan MaxAge { get; private set; }

		public T GetPayload<T>()
		{
			return (T)_payload;
		}

		public object GetPayload()
		{
			return _payload;
		}
	}
}