﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageConnectorTests.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the MessageConnectorTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Implementations.Tests
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Reactive.Subjects;
	using System.Threading.Tasks;

	using ComponentManager.Contracts;

	using NUnit.Framework;

	public sealed class MessageConnectorTests
	{
		private MessageConnectorTests()
		{
		}

		public class GivenAMessageConnector
		{
			private MessageConnector _sut;
			private Subject<IEnvelope> _subject;
			private TestHandler _handler;

			[SetUp]
			public void Setup()
			{
				_subject = new Subject<IEnvelope>();
				var map = new List<KeyValuePair<string, Type>>
							  {
								  new KeyValuePair<string, Type>("test", typeof(ComponentParameters))
							  }
							  .ToLookup(x => x.Key, x => x.Value);
				_handler = new TestHandler();

				_sut = new MessageConnector(_subject, map, t => new[] { _handler });
			}

			[TearDown]
			public void Teardown()
			{
				_sut.Dispose();
			}

			[Test]
			public void WhenInvokingHandlersThenInvokesHandlers()
			{
				var envelope = new TestEnvelope("test", "test", "test", "test", DateTime.UtcNow, TimeSpan.MaxValue, new ComponentParameters("queue", "receive", "blah", string.Empty, string.Empty, IntPtr.Zero));
				_subject.OnNext(envelope);

				Assert.IsTrue(_handler.IsInvoked);
			}
		}

		private class TestHandler : IPayloadHandler
		{
			public bool IsInvoked { get; private set; }

			public Task Handle<T>(T payload)
			{
				IsInvoked = true;
				return Task.FromResult(0);
			}
		}
	}
}