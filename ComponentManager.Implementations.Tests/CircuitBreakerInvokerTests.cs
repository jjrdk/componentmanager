﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CircuitBreakerInvokerTests.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the CircuitBreakerInvokerTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Implementations.Tests
{
	using System;
	using System.Threading.Tasks;

	using NUnit.Framework;

	public sealed class CircuitBreakerInvokerTests
	{
		private CircuitBreakerInvokerTests()
		{
		}

		public class GivenACircuitBreakerInvoker
		{
			private CircuitBreakerInvoker _sut;

			[SetUp]
			public void Setup()
			{
				_sut = new CircuitBreakerInvoker(2, TimeSpan.FromSeconds(5));
			}

			[TearDown]
			public void Teardown()
			{
				_sut.Dispose();
			}

			[Test]
			public async Task WhenExecutingActionThenSucceeds()
			{
				var result = false;
				await _sut.Execute(() => { result = true; });

				Assert.IsTrue(result);
			}

			[Test]
			public async Task WhenExecutingFunctionThenSucceeds()
			{
				var result = await _sut.Execute(() => true);

				Assert.IsTrue(result);
			}
		}
	}
}
