// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObservableSocketClient.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ObservableSocketClient type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Websockets
{
	using System;
	using System.IO;
	using System.Reactive.Linq;
	using System.Reactive.Subjects;
	using System.Security;

	using ComponentManager.Contracts;

	using Newtonsoft.Json;

	using WebSocketSharp;

	public sealed class ObservableSocketClient : IObservable<IEnvelope>, IDisposable
	{
		private readonly Subject<IEnvelope> _messageSubject = new Subject<IEnvelope>();
		private readonly WebSocket _queue;
		private readonly JsonSerializer _serializer;
		private readonly IDisposable _messageSubscription;
		private bool _isDisposed;

		[SecuritySafeCritical]
		public ObservableSocketClient(string endPoint)
		{
			_serializer = new JsonSerializer();
			_serializer.Converters.Add(new SocketEnvelopeConverter());
			_queue = new WebSocket(endPoint);
			_messageSubscription = Observable.FromEventPattern<MessageEventArgs>(
				h => _queue.OnMessage += h,
				h => _queue.OnMessage -= h)
				.Select(x => x.EventArgs)
				.Where(x => x.Type == Opcode.Text || x.Type == Opcode.Binary)
				.Subscribe(OnMessage);
		}

		~ObservableSocketClient()
		{
			Dispose(false);
		}

		public void Send(string data)
		{
			_queue.Send(data);
		}

		/// <summary>
		/// Notifies the provider that an observer is to receive notifications.
		/// </summary>
		/// <returns>
		/// A reference to an interface that allows observers to stop receiving notifications before the provider has finished sending them.
		/// </returns>
		/// <param name="observer">The object that is to receive notifications.</param>
		[SecuritySafeCritical]
		public IDisposable Subscribe(IObserver<IEnvelope> observer)
		{
			return _messageSubject.Subscribe(observer);
		}

		[SecuritySafeCritical]
		public void Start()
		{
			_queue.Connect();
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		[SecuritySafeCritical]
		private void Dispose(bool disposing)
		{
			if (_isDisposed)
			{
				return;
			}

			if (disposing)
			{
				_messageSubscription.Dispose();
				_queue.Close();
				_messageSubject.Dispose();
				_isDisposed = true;
			}

			// get rid of unmanaged resources
		}

		private void OnMessage(MessageEventArgs e)
		{
			IEnvelope envelope = null;

			if (e.Type == Opcode.Binary)
			{
				var rawdata = e.RawData;
				using (var memoryStream = new MemoryStream(rawdata))
				{
					var reader = new StreamReader(memoryStream);
					var jsonTextReader = new JsonTextReader(reader);
					envelope = _serializer.Deserialize<SocketEnvelope>(jsonTextReader);
				}
			}
			else if (e.Type == Opcode.Text)
			{
				using (var reader = new StringReader(e.Data))
				{
					var jsonTextReader = new JsonTextReader(reader);
					envelope = _serializer.Deserialize<SocketEnvelope>(jsonTextReader);
				}
			}

			if (envelope != null)
			{
				_messageSubject.OnNext(envelope);
			}
		}
	}
}