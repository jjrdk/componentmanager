// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SocketEnvelopeConverter.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the SocketEnvelopConverter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Websockets
{
	using System;
	using System.Linq;
	using System.Reflection;

	using Newtonsoft.Json;
	using Newtonsoft.Json.Linq;

	internal class SocketEnvelopeConverter : JsonConverter
	{
		private static readonly PropertyInfo[] TypeProperties;

		static SocketEnvelopeConverter()
		{
			TypeProperties = typeof(SocketEnvelope).GetProperties().Where(x => x.Name != "Body").ToArray();
		}

		/// <summary>
		/// Writes the JSON representation of the object.
		/// </summary>
		/// <param name="writer">The <see cref="T:Newtonsoft.Json.JsonWriter"/> to write to.</param><param name="value">The value.</param><param name="serializer">The calling serializer.</param>
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			var envelope = (SocketEnvelope)value;

			writer.WriteStartObject();
			writer.WritePropertyName("Body");
			writer.WriteRawValue(envelope.Body);
			
			foreach (var property in TypeProperties)
			{
				writer.WritePropertyName(property.Name);
				writer.WriteValue(property.GetValue(envelope));
			}

			writer.WriteEndObject();
		}

		/// <summary>
		/// Reads the JSON representation of the object.
		/// </summary>
		/// <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader"/> to read from.</param><param name="objectType">Type of the object.</param><param name="existingValue">The existing value of object being read.</param><param name="serializer">The calling serializer.</param>
		/// <returns>
		/// The object value.
		/// </returns>
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var json = JObject.Load(reader);

			var maxAge = TimeSpan.Parse(json.Value<string>("MaxAge"));
			var body = json["Body"];
			return new SocketEnvelope(
				json.Value<string>("ID"),
				json.Value<string>("CorrelationID"),
				json.Value<string>("Target"),
				json.Value<string>("Subject"),
				json.Value<DateTime>("Sent"),
				maxAge,
				json.Value<string>("PayloadType"),
				body.ToString());
		}

		/// <summary>
		/// Determines whether this instance can convert the specified object type.
		/// </summary>
		/// <param name="objectType">Type of the object.</param>
		/// <returns>
		/// <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
		/// </returns>
		public override bool CanConvert(Type objectType)
		{
			return typeof(SocketEnvelope).IsAssignableFrom(objectType);
		}
	}
}