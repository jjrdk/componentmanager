// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObservableSocketServer.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the ObservableSocket type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Websockets
{
	using System;
	using System.IO;
	using System.Reactive.Subjects;
	using System.Security;

	using ComponentManager.Contracts;

	using Newtonsoft.Json;

	using WebSocketSharp;
	using WebSocketSharp.Server;

	public sealed class ObservableSocketServer : IObservable<IEnvelope>, IDisposable
	{
		private readonly Subject<IEnvelope> _messageSubject = new Subject<IEnvelope>();
		private readonly WebSocketServer _queue;
		private bool _isDisposed;

		[SecuritySafeCritical]
		public ObservableSocketServer(string endPoint)
		{
			Func<WebSocketBehavior> func = () => new PublishService(_messageSubject);
			var serverUri = new UriBuilder(endPoint);
			var builder = new UriBuilder(serverUri.Scheme, serverUri.Host, serverUri.Port);
			var path = serverUri.Path;
			_queue = new WebSocketServer(builder.ToString());
			_queue.AddWebSocketService(path, func);
		}

		~ObservableSocketServer()
		{
			Dispose(false);
		}

		public void Send(string data)
		{
			_queue.WebSocketServices.Broadcast(data);
		}

		/// <summary>
		/// Notifies the provider that an observer is to receive notifications.
		/// </summary>
		/// <returns>
		/// A reference to an interface that allows observers to stop receiving notifications before the provider has finished sending them.
		/// </returns>
		/// <param name="observer">The object that is to receive notifications.</param>
		[SecuritySafeCritical]
		public IDisposable Subscribe(IObserver<IEnvelope> observer)
		{
			return _messageSubject.Subscribe(observer);
		}

		[SecuritySafeCritical]
		public void Start()
		{
			_queue.Start();
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		[SecuritySafeCritical]
		private void Dispose(bool disposing)
		{
			if (_isDisposed)
			{
				return;
			}

			if (disposing)
			{
				_queue.Stop();
				_messageSubject.Dispose();
				_isDisposed = true;
			}

			// get rid of unmanaged resources
		}

		private class PublishService : WebSocketBehavior
		{
			private readonly ISubject<IEnvelope> _messageSubject;
			private readonly JsonSerializer _serializer;

			public PublishService(ISubject<IEnvelope> messageSubject)
			{
				_messageSubject = messageSubject;
				_serializer = new JsonSerializer();
				_serializer.Converters.Add(new SocketEnvelopeConverter());
			}

			protected override void OnMessage(MessageEventArgs e)
			{
				IEnvelope envelope = null;

				if (e.Type == Opcode.Binary)
				{
					var rawdata = e.RawData;
					using (var memoryStream = new MemoryStream(rawdata))
					{
						var reader = new StreamReader(memoryStream);
						var jsonTextReader = new JsonTextReader(reader);
						envelope = _serializer.Deserialize<SocketEnvelope>(jsonTextReader);
					}
				}
				else if (e.Type == Opcode.Text)
				{
					using (var reader = new StringReader(e.Data))
					{
						var jsonTextReader = new JsonTextReader(reader);
						envelope = _serializer.Deserialize<SocketEnvelope>(jsonTextReader);
					}
				}

				if (envelope != null)
				{
					_messageSubject.OnNext(envelope);
				}
			}
		}
	}
}