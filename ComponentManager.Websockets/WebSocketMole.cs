﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebSocketMole.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the WebSocketMole type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Websockets
{
	using System;
	using System.Security;

	using ComponentManager.Contracts;

	using Newtonsoft.Json;

	[Serializable]
	[SecuritySafeCritical]
	public sealed class WebSocketMole : IObservable<IEnvelope>, IDisposable
	{
		[SecuritySafeCritical]
		private readonly ObservableSocketServer _source;

		[SecuritySafeCritical]
		public WebSocketMole(string receiveEndpoint)
		{
			_source = new ObservableSocketServer(receiveEndpoint);
		}

		~WebSocketMole()
		{
			Dispose(false);
		}

		[SecuritySafeCritical]
		public void Start()
		{
			_source.Start();
		}

		[SecuritySafeCritical]
		public void Publish(string target, string correlationId, string subject, TimeSpan maxAge, object item)
		{
			var envelope = new SocketEnvelope(
				Guid.NewGuid().ToString("N"),
				correlationId,
				target,
				subject,
				DateTime.UtcNow,
				maxAge,
				item);

			var data = JsonConvert.SerializeObject(envelope);
			_source.Send(data);
		}

		/// <summary>
		/// Notifies the provider that an observer is to receive notifications.
		/// </summary>
		/// <returns>
		/// A reference to an interface that allows observers to stop receiving notifications before the provider has finished sending them.
		/// </returns>
		/// <param name="observer">The object that is to receive notifications.</param>
		[SecuritySafeCritical]
		public IDisposable Subscribe(IObserver<IEnvelope> observer)
		{
			return _source.Subscribe(observer);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		[SecuritySafeCritical]
		private void Dispose(bool disposing)
		{
			if (disposing)
			{
				_source.Dispose();
			}
		}
	}
}
