﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebSocketMessageBroker.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the WebSocketMessageBroker type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ComponentManager.Websockets
{
	using System;
	using System.Reactive.Linq;
	using System.Reactive.Subjects;
	using System.Security;

	using ComponentManager.Contracts;

	using Linq2Rest;

	using Newtonsoft.Json;

	using WebSocketSharp;

	public class WebSocketMessageBroker : IMessageBroker
	{
		private readonly ODataExpressionConverter _converter;
		private readonly Subject<IEnvelope> _messageSubject;

		public WebSocketMessageBroker()
		{
			_converter = new ODataExpressionConverter();
			_messageSubject = new Subject<IEnvelope>();
		}

		~WebSocketMessageBroker()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		public void Start()
		{
		}

		public void Send(IEnvelope envelope)
		{
			var socketEnvelope = new SocketEnvelope(
				envelope.ID,
				envelope.CorrelationID,
				envelope.Target,
				envelope.Subject,
				envelope.Sent,
				envelope.MaxAge,
				envelope.GetPayload());
			_messageSubject.OnNext(socketEnvelope);
		}

		public IDisposable Subscribe(SubscriptionRequest subscriptionRequest)
		{
			var filterExpression = _converter.Convert<IEnvelope>(subscriptionRequest.Filter);
			var predicate = filterExpression.Compile();
			return _messageSubject
				.Where(IsNotExpired)
				.Where(predicate)
				.Subscribe(new MessageHandler(subscriptionRequest.EndPoint, _messageSubject));
		}

		private static bool IsNotExpired(IEnvelope x)
		{
			try
			{
				return x.MaxAge == TimeSpan.MaxValue || (x.Sent.ToUniversalTime() + x.MaxAge) >= DateTime.UtcNow;
			}
			catch
			{
				return true;
			}
		}

		private void Dispose(bool disposing)
		{
			if (disposing)
			{
				_messageSubject.Dispose();
			}
		}

		private class MessageHandler : IObserver<IEnvelope>, IDisposable
		{
			private readonly ISubject<IEnvelope> _messageSubject;
			private readonly WebSocket _queue;
			private readonly IDisposable _subscription;
			private readonly SocketEnvelopeConverter _converter;

			public MessageHandler(string endPoint, ISubject<IEnvelope> messageSubject)
			{
				_converter = new SocketEnvelopeConverter();
				_messageSubject = messageSubject;
				_queue = new WebSocket(endPoint);
				_subscription = Observable
					.FromEventPattern<MessageEventArgs>(h => _queue.OnMessage += h, h => _queue.OnMessage -= h)
					.Select(x => x.EventArgs)
					.Where(x => x.Type == Opcode.Text)
					.Select(x => x.Data)
					.Subscribe(OnSocketMessage);
				_queue.Connect();
			}

			~MessageHandler()
			{
				Dispose(false);
			}

			public void Dispose()
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}

			/// <summary>
			/// Provides the observer with new data.
			/// </summary>
			/// <param name="value">The current notification information.</param>
			[SecuritySafeCritical]
			public void OnNext(IEnvelope value)
			{
				var msg = value as SocketEnvelope;
				if (msg == null)
				{
					var body = value.GetPayload();
					msg = new SocketEnvelope(value.ID, value.CorrelationID, value.Target, value.Subject, value.Sent, value.MaxAge, body);
				}

				var json = JsonConvert.SerializeObject(msg, _converter);
				_queue.Send(json);
			}

			/// <summary>
			/// Notifies the observer that the provider has experienced an error condition.
			/// </summary>
			/// <param name="error">An object that provides additional information about the error.</param>
			public void OnError(Exception error)
			{
			}

			/// <summary>
			/// Notifies the observer that the provider has finished sending push-based notifications.
			/// </summary>
			public void OnCompleted()
			{
				_queue.Close();
			}

			private void OnSocketMessage(string e)
			{
				var envelope = JsonConvert.DeserializeObject<SocketEnvelope>(e, _converter);
				_messageSubject.OnNext(envelope);
			}

			private void Dispose(bool disposing)
			{
				if (disposing)
				{
					_subscription.Dispose();
					_queue.Close();
				}
			}
		}
	}
}